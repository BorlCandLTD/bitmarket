<?php defined('SYSPATH') OR die('No direct script access.');

class Controller_Widget_Sidebar_Brands extends Controller_Widget
{
	public function action()
	{
		$this->content->brands = (new Model_Brand)->getArray();
	}
} // End Controller_Widget_Sidebar_Brands