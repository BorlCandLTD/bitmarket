<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * Backend catalog controller
 * 
 */
class Controller_Ajax_Cart extends Controller_Ajax
{

	public function action_index()
	{

	}

	public function action_add()
	{
		# code...
		//$this->content = $_POST;
		
		$this->content = Model::factory('Category')->addCategory($_POST);

	}
	public function action_edit()
	{
		# code...
		//$this->content = $_POST;
		
		$this->content = Model::factory('Category')->editCategory($_POST);

	}
}