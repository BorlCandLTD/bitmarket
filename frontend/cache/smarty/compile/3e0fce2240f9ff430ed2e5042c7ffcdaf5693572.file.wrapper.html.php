<?php /* Smarty version Smarty-3.1.18, created on 2015-01-09 17:43:44
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\common\views\layout\wrapper.html" */ ?>
<?php /*%%SmartyHeaderCode:30224542c5ec9511560-89937241%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e0fce2240f9ff430ed2e5042c7ffcdaf5693572' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\common\\views\\layout\\wrapper.html',
      1 => 1416839220,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30224542c5ec9511560-89937241',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_542c5ec95b1b29_48125489',
  'variables' => 
  array (
    'config' => 0,
    'name' => 0,
    'attributes' => 0,
    'content' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542c5ec95b1b29_48125489')) {function content_542c5ec95b1b29_48125489($_smarty_tpl) {?><!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="<?php echo I18n::lang();?>
"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="<?php echo I18n::lang();?>
"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="<?php echo I18n::lang();?>
"><![endif]-->
<!--[if gt IE 8]><!--><html lang="<?php echo I18n::lang();?>
"><!--<![endif]-->
<head>
	<base href="<?php echo URL::base();?>
">
	
	<?php  $_smarty_tpl->tpl_vars['attributes'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['attributes']->_loop = false;
 $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['config']->value['meta_tags']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['attributes']->key => $_smarty_tpl->tpl_vars['attributes']->value) {
$_smarty_tpl->tpl_vars['attributes']->_loop = true;
 $_smarty_tpl->tpl_vars['name']->value = $_smarty_tpl->tpl_vars['attributes']->key;
?>
		<?php echo HTML::meta($_smarty_tpl->tpl_vars['name']->value,$_smarty_tpl->tpl_vars['attributes']->value);?>

	<?php } ?>

	<link rel="shortcut icon" type="image/png" href="<?php echo CMS::URL_IMG;?>
favicon.ico" />

    <link href="/assets/frontend/css/bootstrap.min.css" rel="stylesheet">

	<?php echo Assets::get('css');?>


	<?php echo Assets::get('js');?>

	<!--Disabled till it will not minimize all in one-->
    <!--<link href="/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/frontend/css/prettyPhoto.css" rel="stylesheet">
    <link href="/assets/frontend/css/price-range.css" rel="stylesheet">
    <link href="/assets/frontend/css/animate.css" rel="stylesheet">
	<link href="/assets/frontend/css/main.css" rel="stylesheet">
	<link href="/assets/frontend/css/responsive.css" rel="stylesheet">
	-->
    <!--[if lt IE 9]>
    <script src="/assets/js/html5shiv.js"></script>
    <script src="/assets/js/respond.min.js"></script>
    <![endif]-->       
	
</head>
<body id="<?php echo @constant('APP');?>
">
	<?php echo (($tmp = @$_smarty_tpl->tpl_vars['content']->value)===null||$tmp==='' ? '' : $tmp);?>

	
	<!--<script src="/assets/frontend/js/bootstrap.min.js"></script>
	<script src="/assets/frontend/js/jquery.scrollUp.min.js"></script>
	<script src="/assets/frontend/js/price-range.js"></script>
    <script src="/assets/frontend/js/jquery.prettyPhoto.js"></script>
    <script src="/assets/frontend/js/main.js"></script>-->
</body>
</html><?php }} ?>
