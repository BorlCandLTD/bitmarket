<?php /* Smarty version Smarty-3.1.18, created on 2014-10-02 00:06:31
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\frontend\views\snippet\header\bottom.html" */ ?>
<?php /*%%SmartyHeaderCode:7948542c5ec79c1eb7-49180325%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5a1089630b80bf14df6fa522e9f2448daa0db7ed' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\frontend\\views\\snippet\\header\\bottom.html',
      1 => 1411648412,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7948542c5ec79c1eb7-49180325',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_542c5ec79c89f8_97456762',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542c5ec79c89f8_97456762')) {function content_542c5ec79c89f8_97456762($_smarty_tpl) {?><div class="header-bottom"><!--header-bottom-->
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="mainmenu pull-left">
					<ul class="nav navbar-nav collapse navbar-collapse">
						<li><a href="index.html" class="active">Home</a></li>
						<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
	                        <ul role="menu" class="sub-menu">
	                            <li><a href="shop.html">Products</a></li>
								<li><a href="product-details.html">Product Details</a></li> 
								<li><a href="checkout.html">Checkout</a></li> 
								<li><a href="cart.html">Cart</a></li> 
								<li><a href="login.html">Login</a></li> 
	                        </ul>
	                    </li> 
						<li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
	                        <ul role="menu" class="sub-menu">
	                            <li><a href="blog.html">Blog List</a></li>
								<li><a href="blog-single.html">Blog Single</a></li>
	                        </ul>
	                    </li> 
						<li><a href="404.html">404</a></li>
						<li><a href="contact-us.html">Contact</a></li>
						
					</ul>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="search_box pull-right">
					<input type="text" placeholder="Search"/>
				</div>
			</div>
		</div>
	</div>
</div><!--/header-bottom--><?php }} ?>
