<?php /* Smarty version Smarty-3.1.18, created on 2014-10-02 00:06:31
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\frontend\views\widget\carousel.html" */ ?>
<?php /*%%SmartyHeaderCode:4181542c5ec79fa1b0-79266186%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c1f4da2809e672ad37d345107ed7004bf8c39fc7' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\frontend\\views\\widget\\carousel.html',
      1 => 1411460178,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4181542c5ec79fa1b0-79266186',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_542c5ec79fe649_98520829',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542c5ec79fe649_98520829')) {function content_542c5ec79fe649_98520829($_smarty_tpl) {?><section id="slider">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div id="slider-carousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#slider-carousel" data-slide-to="1"></li>
						<li data-target="#slider-carousel" data-slide-to="2"></li>
					</ol>
					
					<div class="carousel-inner">
						<div class="item active">
							<div class="col-sm-6">
								<h1><span>BIT</span>Market</h1>
								<h2>Title 1</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
								<button type="button" class="btn btn-default get">Get it now</button>
							</div>
							<div class="col-sm-6">
								<img src="/assets/img/home/girl1.jpg" class="girl img-responsive" alt="" />
								<img src="/assets/img/home/pricing.png"  class="pricing" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="col-sm-6">
								<h1><span>BIT</span>Market</h1>
								<h2>Title 2</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
								<button type="button" class="btn btn-default get">Get it now</button>
							</div>
							<div class="col-sm-6">
								<img src="/assets/img/home/girl2.jpg" class="girl img-responsive" alt="" />
								<img src="/assets/img/home/pricing.png"  class="pricing" alt="" />
							</div>
						</div>
						
						<div class="item">
							<div class="col-sm-6">
								<h1><span>BIT</span>Market</h1>
								<h2>Title 3</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
								<button type="button" class="btn btn-default get">Get it now</button>
							</div>
							<div class="col-sm-6">
								<img src="/assets/img/home/girl3.jpg" class="girl img-responsive" alt="" />
								<img src="/assets/img/home/pricing.png" class="pricing" alt="" />
							</div>
						</div>
						
					</div>
					
					<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>
					<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>
				</div>
				
			</div>
		</div>
	</div>
</section><?php }} ?>
