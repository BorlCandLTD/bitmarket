$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse')
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse')
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    })
})

function setBootstrapStyle (target, tag, style, clearClass) {
    if (clearClass === true)
        target.removeClass();
    var styles = [  tag+"-primary",
                    tag+"-default",
                    tag+"-danger",
                    tag+"-warning",
                    tag+"-success",
                    tag+"-info",
                    //"fa",
                 ];
    $(target).removeClass(styles.join(" ")).addClass(style.join(" "));
    //console.log(target.className);
}

$(function() {
    $("form#addCategory .submit" ).click(function( e ) {
        e.preventDefault();
        //alert(e.target.className);
        setBootstrapStyle(e.target, "btn", Array ("btn-warning"));
        $(e.target).button('progress');

        console.log("Sent to AJAX Controller: " + $(e.target).closest("form").serializeArray());

        $(e.target).prop("disabled", true); // делаем кнопку недоступной, чтобы избежать повторных нажатий
        console.log($(e.target).closest("form").serializeArray());
        $.ajax({ // описываем наш запрос
                type: "POST", // будем передавать данные через POST
                dataType: "json", // указываем, что нам вернется JSON
                url: "/admin/ajax/catalog/add", // запрос отправляем на контроллер Ajax метод addarticle
                data: $(e.target).closest("form").serializeArray(), // передаем данные из формы
                timeout: 5000
            })
            .done (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-success"));
                $(e.target).button('success');
                
            })
            .fail (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-danger"));
                $(e.target).button('error');
                console.log(response);
            })
            .always (function(response) {
                $(e.target).prop("disabled", false);
                console.log("Got response from AJAX Controller: " + JSON.stringify(response));
            });
    });
});

$(function() {
    $("form#editCategory .submit" ).click(function( e ) {
        e.preventDefault();
        //alert(e.target.className);
        setBootstrapStyle(e.target, "btn", Array ("btn-warning"));
        $(e.target).button('progress');

        console.log("Sent to AJAX Controller: " + $(e.target).closest("form").serializeArray());

        $(e.target).prop("disabled", true); // делаем кнопку недоступной, чтобы избежать повторных нажатий
        console.log($(e.target).closest("form").serializeArray());
        $.ajax({ // описываем наш запрос
                type: "POST", // будем передавать данные через POST
                dataType: "json", // указываем, что нам вернется JSON
                url: "/admin/ajax/catalog/edit", // запрос отправляем на контроллер Ajax метод addarticle
                data: $(e.target).closest("form").serializeArray(), // передаем данные из формы
                timeout: 5000
            })
            .done (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-success"));
                $(e.target).button('success');
                
            })
            .fail (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-danger"));
                $(e.target).button('error');
                console.log(response);
            })
            .always (function(response) {
                $(e.target).prop("disabled", false);
                console.log("Got response from AJAX Controller: " + JSON.stringify(response));
            });
    });
});

$(function() {
    $("form#addItem .submit" ).click(function( e ) {
        e.preventDefault();
        //alert(e.target.className);
        setBootstrapStyle(e.target, "btn", Array ("btn-warning"));
        $(e.target).button('progress');

        console.log("Sent to AJAX Controller: " + $(e.target).closest("form").serializeArray());

        $(e.target).prop("disabled", true); // делаем кнопку недоступной, чтобы избежать повторных нажатий
        console.log($(e.target).closest("form").serializeArray());
        $.ajax({ // описываем наш запрос
                type: "POST", // будем передавать данные через POST
                dataType: "json", // указываем, что нам вернется JSON
                url: "/admin/ajax/product/add", // запрос отправляем на контроллер Ajax метод addarticle
                data: $(e.target).closest("form").serializeArray(), // передаем данные из формы
                timeout: 5000
            })
            .done (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-success"));
                $(e.target).button('success');
                
            })
            .fail (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-danger"));
                $(e.target).button('error');
                console.log(response);
            })
            .always (function(response) {
                $(e.target).prop("disabled", false);
                console.log("Got response from AJAX Controller: " + JSON.stringify(response));
            });
    });
});

$(function() {
    $("form#editItem .submit" ).click(function( e ) {
        e.preventDefault();
        //alert(e.target.className);
        setBootstrapStyle(e.target, "btn", Array ("btn-warning"));
        $(e.target).button('progress');

        console.log("Sent to AJAX Controller: " + $(e.target).closest("form").serializeArray());

        $(e.target).prop("disabled", true); // делаем кнопку недоступной, чтобы избежать повторных нажатий
        console.log($(e.target).closest("form").serializeArray());
        $.ajax({ // описываем наш запрос
                type: "POST", // будем передавать данные через POST
                dataType: "json", // указываем, что нам вернется JSON
                url: "/admin/ajax/product/edit", // запрос отправляем на контроллер Ajax метод addarticle
                data: $(e.target).closest("form").serializeArray(), // передаем данные из формы
                timeout: 5000
            })
            .done (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-success"));
                $(e.target).button('success');
                
            })
            .fail (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-danger"));
                $(e.target).button('error');
                console.log(response);
            })
            .always (function(response) {
                $(e.target).prop("disabled", false);
                console.log("Got response from AJAX Controller: " + JSON.stringify(response));
            });
    });
});
$(function() {
    $(document).on('click', '.btn-add', function (e)
    {
        e.preventDefault();

        var controlForm = $('.multiple-input'),
                currentEntry = $(this).parents('.entry:first'),
                newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('1');

        controlForm.find('.entry:not(:last) .btn-add')
                .removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function (e)
    {
        $(this).parents('.entry:first').remove();

        e.preventDefault();
        return false;
    });
});


$(function() {
    $("form#addOrder .submit" ).click(function( e ) {
        e.preventDefault();
        //alert(e.target.className);
        setBootstrapStyle(e.target, "btn", Array ("btn-warning"));
        $(e.target).button('progress');

        console.log("Sent to AJAX Controller: " + $(e.target).closest("form").serializeArray());

        $(e.target).prop("disabled", true); // делаем кнопку недоступной, чтобы избежать повторных нажатий
        console.log($(e.target).closest("form").serializeArray());
        $.ajax({ // описываем наш запрос
                type: "POST", // будем передавать данные через POST
                dataType: "json", // указываем, что нам вернется JSON
                url: "/admin/ajax/order/add", // запрос отправляем на контроллер Ajax метод addarticle
                data: $(e.target).closest("form").serializeArray(), // передаем данные из формы
                timeout: 5000
            })
            .done (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-success"));
                $(e.target).button('success');
                
            })
            .fail (function(response) {
                setBootstrapStyle(e.target, "btn", Array ("btn-danger"));
                $(e.target).button('error');
                console.log(response);
            })
            .always (function(response) {
                $(e.target).prop("disabled", false);
                console.log("Got response from AJAX Controller: " + JSON.stringify(response));
            });
    });
});