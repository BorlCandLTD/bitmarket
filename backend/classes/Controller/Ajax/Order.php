<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * Backend catalog controller
 * 
 */
class Controller_Ajax_Order extends Controller_Ajax
{

	public function action_index()
	{

	}

	public function action_add()
	{
		# code...
		//$this->content = $_POST;
		
		$this->content = Model::factory('Order')->addOrder($_POST);

	}
	public function action_edit()
	{
		# code...
		//$this->content = $_POST;
		
		$this->content = Model::factory('Order')->editOrder($_POST);

	}
}