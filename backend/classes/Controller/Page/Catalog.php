<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * Backend catalog controller
 * 
 */
class Controller_Page_Catalog extends Controller_Page_AdminORM
{

	public $layout = 'layout/basic';

	/**
	 * Name of ORM model.
	 * If not specified, based on controller name.
	 * 
	 * @var mixed(string|ORM)
	 */
	public $model = 'Category';

	/**
	 * Model fields with request params for find
	 * For switch off auto model loading set this property empty
	 * 
	 * @var array
	 */
	public $model_params = array('id' => 'id'); // , 'active' => 1

	/**
	 * A list of actions that don't need create a model
	 *
	 * @var array
	 */
	public $deny_create_model_actions = array('category', 'tag');

	/**
	 * A list of actions that don't need auto loading(find by id) a model
	 * 
	 * @var array
	 */
	public $deny_load_model_actions = array('list', 'index', 'add', 'deleted', 'edit');
	
	private $limit;
	private $category;
	

	public function action_index()
	{
		# code...
	}

	public function action_list()
	{                
            $delete = Arr::get($_GET, 'delete');
            $disable = Arr::get($_GET, 'disable');
            $state = Arr::get($_GET, 'state') == 1 ? 0 : 1;
            
            if ($delete !== NULL && is_numeric($delete))
            {
                $this->model->where("id","=",$delete)->find()->set('deleted', 1)->save()->clear();
            }
            
            if (($disable !== NULL && $state !==NULL) && (is_numeric($disable) && is_numeric($state)))
            {
                $this->model->where("id","=",$disable)->find()->set('disabled', $state)->save()->clear();
            }
            
            $this->limit = Arr::get($_GET, 'limit', 100);

            $this->content->list = $this->model->where("deleted","=",0)->limit($this->limit)->find_all();	
	}

	public function action_add()
	{
		# code...
	}

	public function action_edit()
	{
            $id = (int)Arr::get($_GET, 'id');

            $this->content->categories = $this->model->find_all();
            $this->content->edit = $this->model->order_by('id', 'desc')->where("id","=",$id)->find();
            
            var_dump($this->content->edit->as_array());
	}

	public function action_deleted()
	{
            $undelete = Arr::get($_GET, 'undelete');
            
            if ($undelete !== NULL && is_numeric($undelete))
            {
                $this->model->where("id","=",$undelete)->find()->set('deleted', 0)->save()->clear();
            }
            
		$this->limit = Arr::get($_GET, 'limit', 100);

                $this->content->list = $this->model->where("deleted", "=", 1)->limit($this->limit)->find_all();
	}

	public function action_settings()
	{
		# code...
	}




	/**
	 * Show all posts
	 * 
	 * @return  void
	 *//*
	public function action_list()
	{
		$this->content->posts = $this->model->find_all();
	}

	/**
	 * Show all posts
	 * 
	 * @return  void
	 *//*
	public function action_index()
	{
		$this->category = $this->request->param('category');
		$this->limit = Arr::get($_GET, 'limit', 10);

		if (isset($this->category) && !empty($this->category)) {
			$model_category = Model::factory('Category', array('name' => $this->category));
			$this->content->items = $model_category->items->find_all();
		}
		else
		{
			$this->content->items = Model::factory('Item')->limit($this->limit)->find_all();	
		}
		/*$category = ORM::factory('Category', 2);
		$brands = $category->brands->find_all()->as_array();
		foreach($brands as $brand)
		{
		    echo $category->name .' - '. $brand->name .'<br />';
		}
		/*$author = ORM::factory('category')->find();
		$books = $author->items->find_all();
		foreach($books as $book)
		{
			echo $book->id .' - '. $book->name .'<br />';
		}*/
/*

		$book = ORM::factory('item', 3);
		echo $book->name; // Разум на торги
		echo $book->categories->id; // 1
		echo $book->categories->name; // Андре Нортон*/
	/*}

	/**
	 * Show current post
	 * 
	 * @return  void
	 *//*
	public function action_post()
	{
		$this->content->post = $this->model;
		$this->content->post_tags = $this->model->tags->find_all();
		$this->content->post_categories = $this->model->categories->find_all();
		/*
		$cat = ORM::factory('Blog_Category');
		$cat->name = 'Root';
		$cat->slug = 'root';
		$cat->make_root();
		*/
	/*}

	/**
	 * Show category info and posts
	 * 
	 * @return  void
	 *//*
	public function action_category()
	{
		// Load category
		$category = ORM::factory('Category', array('id' => $this->param('id'), 'disabled' => 0));
		// Checking loading model
		if ( ! $category->loaded())
		{
			throw HTTP_Exception::factory(404, 'Blog category :name not exists', 
				array(':name' => $this->param('id')));
		}
		$this->content->category = $category->as_array();
		
		$this->config['breadcrumbs'] += array('' => $category->name);
		
		if ($category->meta_title)
			$this->config['meta_tags']['title'] = $category->meta_title;
		else
			$this->config['meta_tags']['title'] += array($category->name);
		
		if ($category->meta_description)
			$this->config['meta_tags']['description'] = $category->meta_description;
		
		if ($category->meta_keywords)
			$this->config['meta_tags']['keywords'] = $category->meta_keywords;
		
		// Load category posts
		//$this->content->posts = $category->posts->where('active', '=', 1)->find_items();
	}

	/**
	 * Show category info and posts
	 * 
	 * @return  void
	 *//*
	public function action_tag()
	{
		// Current category info
		$model = ORM::factory('Blog_Tag');
		$tag = $model
			->where($model->primary_key(), '=', $this->param('id'))
			->and_where('active', '=', 1)
			->find();
		// Checking loading model
		if ( ! $category->loaded())
		{
			throw HTTP_Exception::factory(404, 'Blog tag :name not exists', 
				array(':name' => $this->param('id')));
		}
		$this->content->tag = $tag;
		$this->content->tag_posts = $tag->posts->where('active', '=', 1)->find_items();
	}
*/
}