<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * Backend catalog controller
 * 
 */
class Controller_Page_Product extends Controller_Page_AdminORM
{

	public $layout = 'layout/basic';

	/**
	 * Name of ORM model.
	 * If not specified, based on controller name.
	 * 
	 * @var mixed(string|ORM)
	 */
	public $model = 'Item';

	/**
	 * Model fields with request params for find
	 * For switch off auto model loading set this property empty
	 * 
	 * @var array
	 */
	public $model_params = array('id' => 'id'); // , 'active' => 1

	/**
	 * A list of actions that don't need create a model
	 *
	 * @var array
	 */
	public $deny_create_model_actions = array('category', 'tag');

	/**
	 * A list of actions that don't need auto loading(find by id) a model
	 * 
	 * @var array
	 */
	public $deny_load_model_actions = array('list', 'index', 'deleted', 'edit', 'add');
	
	private $limit;
	private $category;
	
	/**
	 * Show all posts
	 * 
	 * @return  void
	 */
	public function action_list()
	{           
            $delete = Arr::get($_GET, 'delete');
            $disable = Arr::get($_GET, 'disable');
            $state = Arr::get($_GET, 'state') == 1 ? 0 : 1;
            
            if ($delete !== NULL && is_numeric($delete))
            {
                $this->model->where("id","=",$delete)->find()->set('deleted', 1)->save()->clear();
            }
            
            if (($disable !== NULL && $state !==NULL) && (is_numeric($disable) && is_numeric($state)))
            {
                
                $this->model->where("id","=",$disable)->find()->set('disabled', $state)->save()->clear();
            }

            $this->limit = Arr::get($_GET, 'limit', 100);

            $this->content->list = $this->model->where("deleted","=",0)->limit($this->limit)->find_all();	
	}

	public function action_deleted()
	{
            $undelete = Arr::get($_GET, 'undelete');
            
            if ($undelete !== NULL && is_numeric($undelete))
            {
                $this->model->where("id","=",$undelete)->find()->set('deleted', 0)->save()->clear();
            }
            
            $this->limit = Arr::get($_GET, 'limit', 100);

            $this->content->list = $this->model->where("deleted", "=", 1)->limit($this->limit)->find_all();
	}
        
	public function action_edit()
	{
            $this->content->brands = ORM::factory('Brand')->find_all();
            $this->content->categories = ORM::factory('Category')->find_all();
            $id = (int)Arr::get($_GET, 'id');

            $this->content->edit = $this->model->order_by('id', 'desc')->where("id","=",$id)->find();
	}
        
	public function action_add()
	{
            $this->content->brands = ORM::factory('Brand')->find_all();
            $this->content->categories = ORM::factory('Category')->find_all();
	}

	/**
	 * Show all posts
	 * 
	 * @return  void
	 */
	public function action_index()
	{
            # code...
	}
}