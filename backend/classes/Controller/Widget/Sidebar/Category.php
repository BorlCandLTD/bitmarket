<?php defined('SYSPATH') OR die('No direct script access.');

class Controller_Widget_Sidebar_Category extends Controller_Widget
{
	public function action()
	{										
		$this->content->categories = $this->mapTree(Model::factory('Category')->getArray(), 0);
	}

	private function mapTree($dataset, $parent=null) {
 
 		// Debug

 		//return $dataset;

		$tree = array();
		foreach ($dataset as $id=>&$node) 
		{
			if (is_object($node))
				$node = $node->as_array();
			//var_dump($node);
			if ($node['parent_id'] == 0) 
			{ // root node
				$tree[$node['id']] = &$node;
			} 
			else 
			{ // sub node
				if (!isset($tree[$node['parent_id']]['childs'])) 
					$tree[$node['parent_id']]['childs'] = array();
				$tree[$node['parent_id']]['childs'][] = &$node;
			}
		}

		return $tree;

	}
} // End Controller_Widget_Sidebar_Category