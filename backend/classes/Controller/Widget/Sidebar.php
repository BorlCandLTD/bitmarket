<?php defined('SYSPATH') OR die('No direct script access.');

class Controller_Widget_Sidebar extends Controller_Widget
{

	public function getClassActions($class, $get_index_action = FALSE)
	{
		$actions = array();

		foreach ((array)get_class_methods($class) as $method) {
			$action_prefix = substr($method, 0, 7);
			$action = substr($method, 7);
			if ($action_prefix === 'action_')
			{
				if ($action === 'index' && $get_index_action === FALSE)
					continue;
				$actions[] = $action;
			}
		}

		return $actions;
	}

	public function action()
	{
		// ..
	}
} // End Controller_Widget_Sidebar