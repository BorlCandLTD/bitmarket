<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Blog posts category model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Brand extends ORM {

	public function getArray(){

		return ORM::factory('Brand')
			->order_by('name', 'asc')
			//->limit(1)
			->cached(Date::MINUTE)
			->find_all()
			->as_array();

	}
}