<?php /* Smarty version Smarty-3.1.18, created on 2015-01-07 16:13:02
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\backend\views\page\catalog\edit.html" */ ?>
<?php /*%%SmartyHeaderCode:1602754afeb24009799-22116216%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ae7ad3e0a5f63a096eace78ec1bd305698880ea4' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\backend\\views\\page\\catalog\\edit.html',
      1 => 1420885700,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1602754afeb24009799-22116216',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54afeb24085596_20839053',
  'variables' => 
  array (
    'edit' => 0,
    'categories' => 0,
    'category' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54afeb24085596_20839053')) {function content_54afeb24085596_20839053($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_i18n')) include 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\common\\vendor\\smarty\\plugins\\modifier.i18n.php';
?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <ol class="breadcrumb">
                <li><a href="#">Категории</a></li>
                <li class="active">Изменить</li>
                <?php if (isset($_smarty_tpl->tpl_vars['edit']->value)) {?> <li class="active">Категория <?php echo $_smarty_tpl->tpl_vars['edit']->value->name;?>
 (<?php echo $_smarty_tpl->tpl_vars['edit']->value->id;?>
)</li> <?php }?>
            </ol>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php if ((isset($_smarty_tpl->tpl_vars['edit']->value)&&$_smarty_tpl->tpl_vars['edit']->value->id!==null)) {?>
<div class="row">
    <div class="table-responsive">
        <form role="form" class="text-center" id="editCategory">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col-xs-12">

                        <div class="form-group" >
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-tags"></i> Category name
                                </span>
                                <input autocomplete='off' type="text" name="name" class="form-control bg-danger" placeholder="Category name" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value->name;?>
" required>
                                
                                <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value->id;?>
" name="id">
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fw fa-level-up"></i>Parent category</span>
                                <select autocomplete='off' name="parent_id" class="form-control">
                                    <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
" <?php echo $_smarty_tpl->tpl_vars['category']->value->id==$_smarty_tpl->tpl_vars['edit']->value->parent_id ? 'selected' : '';?>
 ><?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
 - <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
<?php $_tmp1=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp1);?>
 <?php echo $_smarty_tpl->tpl_vars['category']->value->disabled==1 ? '[DIS]' : '';?>
 <?php echo $_smarty_tpl->tpl_vars['category']->value->deleted==1 ? '[DEL]' : '';?>
 </option>
                                    <?php } ?>
                                </select>
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fw fa-sort-numeric-asc"></i> Show order</span>
                                <select autocomplete='off' name="sort_order" class="form-control">
                                    <option>0</option>
                                    <option>1</option>
                                </select>
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default disabled">
                                    <i class="fa fa-fw fa-ban"></i> Disabled
                                </label>

                                <label class="btn btn-success transition<?php echo $_smarty_tpl->tpl_vars['edit']->value->disabled==0 ? ' active' : '';?>
">
                                    <input class="form-control" type="radio" name="disabled" value="false" autocomplete="off" <?php echo $_smarty_tpl->tpl_vars['edit']->value->disabled==0 ? 'checked' : '';?>
> No
                                </label>

                                <label class="btn btn-danger transition<?php echo $_smarty_tpl->tpl_vars['edit']->value->disabled==1 ? ' active' : '';?>
">
                                    <input class="form-control" type="radio" name="disabled" value="true" autocomplete="off" <?php echo $_smarty_tpl->tpl_vars['edit']->value->disabled==1 ? 'checked' : '';?>
> Yes
                                </label>
                            </div>

                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default disabled">
                                    <i class="fa fa-fw fa-trash"></i> Deleted
                                </label>

                                <label class="btn btn-success transition<?php echo $_smarty_tpl->tpl_vars['edit']->value->deleted==0 ? ' active' : '';?>
">
                                    <input class="form-control" type="radio" name="deleted" value="false" autocomplete="off" <?php echo $_smarty_tpl->tpl_vars['edit']->value->deleted==0 ? 'checked' : '';?>
> No
                                </label>

                                <label class="btn btn-danger transition<?php echo $_smarty_tpl->tpl_vars['edit']->value->deleted==1 ? ' active' : '';?>
">
                                    <input class="form-control" type="radio" name="deleted" value="true" autocomplete="off" <?php echo $_smarty_tpl->tpl_vars['edit']->value->deleted==1 ? 'checked' : '';?>
> Yes
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-fw fa-file-text"></i> Description
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                        <textarea autocomplete='off' name="description" class="form-control noresize" rows="3"><?php echo $_smarty_tpl->tpl_vars['edit']->value->description;?>
</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-fw fa-key"></i> Keywords
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                        <textarea autocomplete='off' name="keywords" class="form-control noresize" rows="3"><?php echo $_smarty_tpl->tpl_vars['edit']->value->keywords;?>
</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">

                <button type="button" autocomplete='off' class="btn btn-default transition btn-circle btn-xl">
                    <i class="fa fa-arrow-left"></i>
                </button>

                <button type="submit" id="submit" autocomplete='off' data-progress-text="<i class='fa fa-refresh fa-spin'></i>" data-success-text="<i class='fa fa-check'></i>" data-error-text="<i class='fa fa-times'></i>" class="submit btn btn-primary btn-circle btn-xl transition">
                    <i class="fa fa-pencil"></i>
                </button>

            </div>
        </form>
    </div>
</div>
<?php } else { ?>
<div class="row">
	<div class="col-lg-offset-4 col-md-offset-2 col-lg-4 col-md-8">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-warning fa-lg"></i> Error</h3>
			</div>
			<div class="panel-body">
				There is no category to edit with this id
			</div>
		</div>
	</div>
</div>
<?php }?><?php }} ?>
