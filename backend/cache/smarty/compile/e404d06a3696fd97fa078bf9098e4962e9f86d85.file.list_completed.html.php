<?php /* Smarty version Smarty-3.1.18, created on 2015-01-12 22:08:39
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\backend\views\page\order\list_completed.html" */ ?>
<?php /*%%SmartyHeaderCode:2742654b3fce73197f5-83487789%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e404d06a3696fd97fa078bf9098e4962e9f86d85' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\backend\\views\\page\\order\\list_completed.html',
      1 => 1421086117,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2742654b3fce73197f5-83487789',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54b3fce739f877_38764268',
  'variables' => 
  array (
    'list' => 0,
    'order' => 0,
    'list_items' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54b3fce739f877_38764268')) {function content_54b3fce739f877_38764268($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_i18n')) include 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\common\\vendor\\smarty\\plugins\\modifier.i18n.php';
?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <ol class="breadcrumb">
                <li><a href="#">Заказы</a></li>
                <li class="active">Список</li>
            </ol>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">

            <thead>
                <tr>
                    <th>#</th>
                    <th>ID клиента</th>
                    <th>Дата заказа</th>
                    <th>Дата оплаты</th>
                    <th>Сумма</th>
                    <th>Действия</th>
                </tr>
            </thead>

            <tbody>
                <?php  $_smarty_tpl->tpl_vars['order'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['order']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['order']->key => $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->_loop = true;
?>
                    <tr data-toggle="collapse" data-target="#order<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
" class="accordion-toggle">
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value->client_id;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value->date_order;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value->date_pay;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['order']->value->summary;?>
</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="">
                                <a class="btn btn-warning btn-xs" href="/admin/order/edit?id="><i class="fa fa-pencil fa-fw"></i> Изменить</a>
                                <a class="btn btn-info btn-xs" href=""><?php ob_start();?><?php echo smarty_modifier_i18n('accept');?>
<?php $_tmp1=ob_get_clean();?><?php ob_start();?><?php echo smarty_modifier_i18n('decline');?>
<?php $_tmp2=ob_get_clean();?><?php echo 1==1 ? "<i class='fa fa-toggle-off fa-fw'></i> ".$_tmp1 : "<i class='fa fa-toggle-on fa-fw'></i> ".$_tmp2;?>
 </a>
                                <a class="btn btn-danger btn-xs" href="/admin/order/list?delete="><i class="fa fa-trash fa-fw"></i> Удалить</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="12" class="hiddenRow">
                            <div class="accordian-body collapse" id="order<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
"> 
                                <table class="table table-striped">
                                    <thead>
                                        <tr><th>#</th><th>Item ID</th><th>Brand</th><th>Item</th><th>Count</th><th>Price</th></tr>
                                    </thead>
                                    <tbody>
                                        <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
<?php $_tmp3=ob_get_clean();?><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_items']->value[$_tmp3]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                                            <tr>
                                                <td><?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['item']->value->item->id;?>
</td>
                                                <td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['item']->value->item->brands->name;?>
<?php $_tmp4=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp4);?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['item']->value->item->name;?>
</td>
                                                <th><?php echo $_smarty_tpl->tpl_vars['item']->value->count;?>
</th>
                                                <td><?php echo $_smarty_tpl->tpl_vars['item']->value->price;?>
</td>
                                            </tr>
                                        
                                        <?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
                                            <tr>
                                                <td colspan="12">There is no products</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                <?php }
if (!$_smarty_tpl->tpl_vars['order']->_loop) {
?>
                    <tr>
                        <td colspan="12">There is no orders</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div><?php }} ?>
