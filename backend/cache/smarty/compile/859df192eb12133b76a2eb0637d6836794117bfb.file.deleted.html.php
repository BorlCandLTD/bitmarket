<?php /* Smarty version Smarty-3.1.18, created on 2015-01-10 02:00:10
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\backend\views\page\product\deleted.html" */ ?>
<?php /*%%SmartyHeaderCode:1391654b044bd6975f4-37049902%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '859df192eb12133b76a2eb0637d6836794117bfb' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\backend\\views\\page\\product\\deleted.html',
      1 => 1420840129,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1391654b044bd6975f4-37049902',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54b044bd732fa0_89833671',
  'variables' => 
  array (
    'list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54b044bd732fa0_89833671')) {function content_54b044bd732fa0_89833671($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_i18n')) include 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\common\\vendor\\smarty\\plugins\\modifier.i18n.php';
?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
	        <ol class="breadcrumb">
	        	<li><a href="#">Товары</a></li>
	        	<li class="active">Удаленные</li>
	        </ol>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Категория</th>
                    <th>Бренд</th>
                    <th>Название товара</th>
                    <th>Товаров в юните</th>
                    <th>Цена</th>
                    <th>Количество на складе</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
</td>
                    <td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['item']->value->categories->name;?>
<?php $_tmp1=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp1);?>
</td>
                    <td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['item']->value->brands->name;?>
<?php $_tmp2=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp2);?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->unit;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->price;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->count;?>
</td>
                    <td>
                        <a class="btn btn-info btn-xs" href="/admin/product/deleted?undelete=<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><i class="fa fa-reply fa-fw"></i> Отменить удаление</a>
                    </td>
                </tr>
            <?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
            	<tr>
            	 <td colspan="6">There is no products</td>
            	</tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div><?php }} ?>
