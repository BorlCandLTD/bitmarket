<?php /* Smarty version Smarty-3.1.18, created on 2015-01-09 19:20:08
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\backend\views\snippet\sidebar.html" */ ?>
<?php /*%%SmartyHeaderCode:23867547a22bb7f7578-85307915%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bcb49460675761e56ef394551897791c1045f28b' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\backend\\views\\snippet\\sidebar.html',
      1 => 1420816805,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '23867547a22bb7f7578-85307915',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_547a22bb899b55_03696058',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_547a22bb899b55_03696058')) {function content_547a22bb899b55_03696058($_smarty_tpl) {?><div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
                <!-- /input-group -->
            </li>
            <li class="active">
                <a href="#"><i class="fa fa-tags fa-fw"></i> Категории<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/admin/catalog/" class="active">Настройки</a>
                    </li>
                    <li>
                        <a href="/admin/catalog/list">Список категорий</a>
                    </li>
                    <li>
                        <a href="/admin/catalog/add">Добавить категорию</a>
                    </li>
                    <!--<li>
                        <a href="/admin/catalog/edit">Редактировать категории</a>
                    </li>-->
                    <li>
                        <a href="/admin/catalog/deleted">Удалённые категории</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-bars fa-fw"></i> Товары<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/admin/product/" class="active">Настройки</a>
                    </li>
                    <li>
                        <a href="/admin/product/list">Список товаров</a>
                    </li>
                    <li>
                        <a href="/admin/product/add">Добавить товар</a>
                    </li>
                    <!--<li>
                        <a href="/admin/catalog/edit">Редактировать категории</a>
                    </li>-->
                    <li>
                        <a href="/admin/product/deleted">Удалённые товары</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <!--<li>
                <a class="active" href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="flot.html">Flot Charts</a>
                    </li>
                    <li>
                        <a href="morris.html">Morris.js Charts</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            <!--</li>
            <li>
                <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
            </li>
            <li>
                <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="panels-wells.html">Panels and Wells</a>
                    </li>
                    <li>
                        <a href="buttons.html">Buttons</a>
                    </li>
                    <li>
                        <a href="notifications.html">Notifications</a>
                    </li>
                    <li>
                        <a href="typography.html">Typography</a>
                    </li>
                    <li>
                        <a href="grid.html">Grid</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            <!--</li>
            <li>
                <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Third Level <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                        </ul>
                        <!-- /.nav-third-level -->
                    <!--</li>
                </ul>
                <!-- /.nav-second-level -->
            <!--</li>
            <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="blank.html">Blank Page</a>
                    </li>
                    <li>
                        <a href="login.html">Login Page</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            <!--</li>-->
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side --><?php }} ?>
