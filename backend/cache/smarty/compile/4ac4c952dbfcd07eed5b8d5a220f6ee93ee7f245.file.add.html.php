<?php /* Smarty version Smarty-3.1.18, created on 2015-01-10 15:00:32
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\backend\views\page\product\add.html" */ ?>
<?php /*%%SmartyHeaderCode:2916854b0fc0c9f8774-44697699%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ac4c952dbfcd07eed5b8d5a220f6ee93ee7f245' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\backend\\views\\page\\product\\add.html',
      1 => 1420887629,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2916854b0fc0c9f8774-44697699',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54b0fc0cae9167_25551197',
  'variables' => 
  array (
    'brands' => 0,
    'brand' => 0,
    'categories' => 0,
    'category' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54b0fc0cae9167_25551197')) {function content_54b0fc0cae9167_25551197($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_i18n')) include 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\common\\vendor\\smarty\\plugins\\modifier.i18n.php';
?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <ol class="breadcrumb">
                <li><a href="#">Товары</a></li>
                <li class="active">Добавить</li>
            </ol>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="table-responsive">
        <form role="form" class="text-center" id="addItem">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col-xs-12">

                        
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fw fa-level-up"></i>Бренд</span>
                                <select autocomplete='off' name="brand_id" class="form-control">
                                    <?php  $_smarty_tpl->tpl_vars['brand'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['brand']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['brands']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['brand']->key => $_smarty_tpl->tpl_vars['brand']->value) {
$_smarty_tpl->tpl_vars['brand']->_loop = true;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['brand']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['brand']->value->id;?>
 - <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['brand']->value->name;?>
<?php $_tmp1=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp1);?>
</option>
                                    <?php } ?>
                                </select>
                            </div> 
                        </div>
                        
                        <div class="form-group" >
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-tags"></i> Название товара
                                </span>
                                <input autocomplete='off' type="text" name="name" class="form-control bg-danger" placeholder="Название товара" required>
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fw fa-level-up"></i>Категория товара</span>
                                <select autocomplete='off' name="category_id" class="form-control">
                                    <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
 - <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
<?php $_tmp2=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp2);?>
 <?php echo $_smarty_tpl->tpl_vars['category']->value->disabled==1 ? '[DIS]' : '';?>
 <?php echo $_smarty_tpl->tpl_vars['category']->value->deleted==1 ? '[DEL]' : '';?>
 </option>
                                    <?php } ?>
                                </select>
                            </div> 
                        </div>

                            <div class="form-group">
                        <div class="row">

                                <div class="col-xs-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-fw fa-sort-numeric-asc"></i> Цена</span>
                                        <input autocomplete='off' type="text" name="price" class="form-control bg-danger" placeholder="Цена" required>
                                    </div> 
                                </div> 
                                <div class="col-xs-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-fw fa-sort-numeric-asc"></i> На складе</span>
                                        <input autocomplete='off' type="text" name="count" class="form-control bg-danger" placeholder="Кол-во на складе" required>
                                    </div> 
                                </div> 
                                <div class="col-xs-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-fw fa-sort-numeric-asc"></i> В юните</span>
                                        <input autocomplete='off' type="text" name="unit" class="form-control bg-danger" placeholder="Кол-во в лоте" required>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default disabled">
                                    <i class="fa fa-fw fa-ban"></i> Disabled
                                </label>

                                <label class="btn btn-success transition active">
                                    <input class="form-control" type="radio" name="disabled" value="false" autocomplete="off" checked> No
                                </label>

                                <label class="btn btn-danger transition">
                                    <input class="form-control" type="radio" name="disabled" value="true" autocomplete="off"> Yes
                                </label>
                            </div>

                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default disabled">
                                    <i class="fa fa-fw fa-trash"></i> Deleted
                                </label>

                                <label class="btn btn-success transition active">
                                    <input class="form-control" type="radio" name="deleted" value="false" autocomplete="off" checked> No
                                </label>

                                <label class="btn btn-danger transition">
                                    <input class="form-control" type="radio" name="deleted" value="true" autocomplete="off"> Yes
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-fw fa-key"></i> Keywords
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                    <textarea autocomplete='off' name="keywords" class="form-control noresize" rows="3"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">

                <button type="button" autocomplete='off' class="btn btn-default transition btn-circle btn-xl">
                    <i class="fa fa-arrow-left"></i>
                </button>

                <button type="submit" id="submit" autocomplete='off' data-progress-text="<i class='fa fa-refresh fa-spin'></i>" data-success-text="<i class='fa fa-check'></i>" data-error-text="<i class='fa fa-times'></i>" class="submit btn btn-primary btn-circle btn-xl transition">
                    <i class="fa fa-pencil"></i>
                </button>

            </div>
        </form>
    </div>
</div><?php }} ?>
