<?php /* Smarty version Smarty-3.1.18, created on 2014-12-07 18:43:10
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\backend\views\page\catalog\add.html" */ ?>
<?php /*%%SmartyHeaderCode:21868547a22bb2c6f65-57198228%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '360e77b5463147adc2e737c1993a4bc49c66050d' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\backend\\views\\page\\catalog\\add.html',
      1 => 1417963381,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21868547a22bb2c6f65-57198228',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_547a22bb46ecc7_54429548',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_547a22bb46ecc7_54429548')) {function content_547a22bb46ecc7_54429548($_smarty_tpl) {?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <ol class="breadcrumb">
                <li><a href="#">Категории</a></li>
                <li class="active">Добавить</li>
            </ol>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="table-responsive">
        <form role="form" class="text-center" id="addCategory">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col-xs-12">

                        <div class="form-group" >
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-tags"></i> Category name
                                </span>
                                <input autocomplete='off' type="text" name="name" class="form-control bg-danger" placeholder="Category name" required>
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fw fa-level-up"></i>Parent category</span>
                                <select autocomplete='off' name="parent_id" class="form-control">
                                    <option>0</option>
                                    <option>1</option>
                                </select>
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fw fa-sort-numeric-asc"></i> Show order</span>
                                <select autocomplete='off' name="sort_order" class="form-control">
                                    <option>0</option>
                                    <option>1</option>
                                </select>
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default disabled">
                                    <i class="fa fa-fw fa-ban"></i> Disabled
                                </label>

                                <label class="btn btn-success transition active">
                                    <input class="form-control" type="radio" name="disabled" value="false" autocomplete="off" checked> No
                                </label>

                                <label class="btn btn-danger transition">
                                    <input class="form-control" type="radio" name="disabled" value="true" autocomplete="off"> Yes
                                </label>
                            </div>

                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default disabled">
                                    <i class="fa fa-fw fa-trash"></i> Deleted
                                </label>

                                <label class="btn btn-success transition active">
                                    <input class="form-control" type="radio" name="deleted" value="false" autocomplete="off" checked> No
                                </label>

                                <label class="btn btn-danger transition">
                                    <input class="form-control" type="radio" name="deleted" value="true" autocomplete="off"> Yes
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-fw fa-file-text"></i> Description
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                        <textarea autocomplete='off' name="description" class="form-control noresize" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-fw fa-key"></i> Keywords
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                        <textarea autocomplete='off' name="keywords" class="form-control noresize" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">

                <button type="button" autocomplete='off' class="btn btn-default transition btn-circle btn-xl">
                    <i class="fa fa-arrow-left"></i>
                </button>

                <button type="submit" id="submit" autocomplete='off' data-progress-text="<i class='fa fa-refresh fa-spin'></i>" data-success-text="<i class='fa fa-check'></i>" data-error-text="<i class='fa fa-times'></i>" class="submit btn btn-primary btn-circle btn-xl transition">
                    <i class="fa fa-pencil"></i>
                </button>

            </div>
        </form>
    </div>
</div><?php }} ?>
