<?php /* Smarty version Smarty-3.1.18, created on 2015-01-10 01:52:55
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\backend\views\page\catalog\deleted.html" */ ?>
<?php /*%%SmartyHeaderCode:466454afeb62cf38a4-79711103%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd9a6ba306ff1399ab957ee0bd38e4929c3355d49' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\backend\\views\\page\\catalog\\deleted.html',
      1 => 1420840051,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '466454afeb62cf38a4-79711103',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54afeb62da04a8_14818192',
  'variables' => 
  array (
    'list' => 0,
    'category' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54afeb62da04a8_14818192')) {function content_54afeb62da04a8_14818192($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_i18n')) include 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\common\\vendor\\smarty\\plugins\\modifier.i18n.php';
?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
	        <ol class="breadcrumb">
	        	<li><a href="#">Категории</a></li>
	        	<li class="active">Удаленные</li>
	        </ol>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Родитель</th>
                    <th>Название категории</th>
                    <th>Унифицированное название категории</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
            <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['category']->value->parent->name;?>
</td>
                    <td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
<?php $_tmp1=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp1);?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
</td>
                    <td>
                        <a class="btn btn-info btn-xs" href="/admin/catalog/deleted?undelete=<?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
"><i class="fa fa-reply fa-fw"></i> Отменить удаление</a>
                    </td>
                </tr>
            <?php }
if (!$_smarty_tpl->tpl_vars['category']->_loop) {
?>
            	<tr>
            	 <td colspan="6">There is no categories</td>
            	</tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div><?php }} ?>
