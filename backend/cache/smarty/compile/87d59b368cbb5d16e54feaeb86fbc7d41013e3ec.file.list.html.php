<?php /* Smarty version Smarty-3.1.18, created on 2015-01-10 01:53:41
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\backend\views\page\product\list.html" */ ?>
<?php /*%%SmartyHeaderCode:358454aff28ad82e33-13992282%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '87d59b368cbb5d16e54feaeb86fbc7d41013e3ec' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\backend\\views\\page\\product\\list.html',
      1 => 1420840418,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '358454aff28ad82e33-13992282',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54aff28ae2aab0_81202945',
  'variables' => 
  array (
    'list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54aff28ae2aab0_81202945')) {function content_54aff28ae2aab0_81202945($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_i18n')) include 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\common\\vendor\\smarty\\plugins\\modifier.i18n.php';
?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
	        <ol class="breadcrumb">
	        	<li><a href="#">Товары</a></li>
	        	<li class="active">Список</li>
	        </ol>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Категория</th>
                    <th>Бренд</th>
                    <th>Название товара</th>
                    <th>Товаров в юните</th>
                    <th>Цена</th>
                    <th>Количество на складе</th>
                    <th>Состояние</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
</td>
                    <td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['item']->value->categories->name;?>
<?php $_tmp1=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp1);?>
</td>
                    <td><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['item']->value->brands->name;?>
<?php $_tmp2=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp2);?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->unit;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->price;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->count;?>
</td>
                    <td><?php ob_start();?><?php echo smarty_modifier_i18n('disabled');?>
<?php $_tmp3=ob_get_clean();?><?php ob_start();?><?php echo smarty_modifier_i18n('enabled');?>
<?php $_tmp4=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['item']->value->disabled==1 ? $_tmp3 : $_tmp4;?>
</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            <a class="btn btn-warning btn-xs" href="/admin/product/edit?id=<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><i class="fa fa-pencil fa-fw"></i> Изменить</a>
                            <a class="btn btn-info btn-xs" href="/admin/product/list?disable=<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
&state=<?php echo $_smarty_tpl->tpl_vars['item']->value->disabled;?>
"><?php ob_start();?><?php echo smarty_modifier_i18n('enable');?>
<?php $_tmp5=ob_get_clean();?><?php ob_start();?><?php echo smarty_modifier_i18n('disable');?>
<?php $_tmp6=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['item']->value->disabled==1 ? "<i class='fa fa-toggle-off fa-fw'></i> ".$_tmp5 : "<i class='fa fa-toggle-on fa-fw'></i> ".$_tmp6;?>
 </a>
                            <a class="btn btn-danger btn-xs" href="/admin/product/list?delete=<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"><i class="fa fa-trash fa-fw"></i> Удалить</a>
                        </div>
                    </td>
                </tr>
            <?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
            	<tr>
            	 <td colspan="6">There is no products</td>
            	</tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div><?php }} ?>
