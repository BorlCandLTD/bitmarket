<?php /* Smarty version Smarty-3.1.18, created on 2015-01-13 11:17:17
         compiled from "R:\Development\DEV_PHP5.6\domains\general.bitmarket.me\backend\views\page\order\add.html" */ ?>
<?php /*%%SmartyHeaderCode:1067554b425104c51c0-30606333%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c83b1718a948bcc7bffad8e1308e129146ba5fe8' => 
    array (
      0 => 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\backend\\views\\page\\order\\add.html',
      1 => 1421133431,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1067554b425104c51c0-30606333',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54b425104fff97_89072299',
  'variables' => 
  array (
    'clients' => 0,
    'client' => 0,
    'brands' => 0,
    'brand' => 0,
    'brand_items' => 0,
    'brand_item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54b425104fff97_89072299')) {function content_54b425104fff97_89072299($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_i18n')) include 'R:\\Development\\DEV_PHP5.6\\domains\\general.bitmarket.me\\common\\vendor\\smarty\\plugins\\modifier.i18n.php';
?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <ol class="breadcrumb">
                <li><a href="#">Заказы</a></li>
                <li class="active">Добавить</li>
            </ol>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<script>
    

</script>
<div class="row">
    <div class="table-responsive">

        <form role="form" class="text-center" id="addOrder">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col-xs-12">


                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-fw fa-level-up"></i>ID клиента</span>
                                <select autocomplete='off' name="client_id" class="form-control">
                                    <?php  $_smarty_tpl->tpl_vars['client'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['client']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['clients']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['client']->key => $_smarty_tpl->tpl_vars['client']->value) {
$_smarty_tpl->tpl_vars['client']->_loop = true;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['client']->value->session_id;?>
"><?php echo $_smarty_tpl->tpl_vars['client']->value->session_id;?>
</option>
                                    <?php } ?>
                                </select>
                            </div> 
                        </div>
                                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-fw fa-key"></i> Комментарий
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                        <textarea autocomplete='off' name="keywords" class="form-control noresize" rows="1"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-fw fa-file-text"></i> Товары в заказе
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                        
                                    <div class="multiple-input controls-row">
                                        <div class="entry input-group col-xs-12">
                                            <select name="items[]['id']" class="form-control">
                                                <?php  $_smarty_tpl->tpl_vars['brand'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['brand']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['brands']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['brand']->key => $_smarty_tpl->tpl_vars['brand']->value) {
$_smarty_tpl->tpl_vars['brand']->_loop = true;
?>
                                                    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['brand']->value->id;?>
<?php $_tmp1=ob_get_clean();?><?php if ((count($_smarty_tpl->tpl_vars['brand_items']->value[$_tmp1])>0)) {?>
                                                        <optgroup label="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['brand']->value->name;?>
<?php $_tmp2=ob_get_clean();?><?php echo smarty_modifier_i18n($_tmp2);?>
">
                                                            <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['brand']->value->id;?>
<?php $_tmp3=ob_get_clean();?><?php  $_smarty_tpl->tpl_vars['brand_item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['brand_item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['brand_items']->value[$_tmp3]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['brand_item']->key => $_smarty_tpl->tpl_vars['brand_item']->value) {
$_smarty_tpl->tpl_vars['brand_item']->_loop = true;
?>
                                                                <option value="<?php echo $_smarty_tpl->tpl_vars['brand_item']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['brand_item']->value->name;?>
</option>
                                                            <?php } ?>
                                                        </optgroup>
                                                    <?php }?>
                                                <?php } ?>
                                            </select>
                                            <span class="input-group-btn" style="width:0px;"></span>
                                            <input name="items[]['count']" type="text" class="form-control" value="1">
                                            <span class="input-group-btn">
                                                <button class="btn btn-success btn-add" type="button">
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row">

                <button type="button" autocomplete='off' class="btn btn-default transition btn-circle btn-xl">
                    <i class="fa fa-arrow-left"></i>
                </button>

                <button type="submit" id="submit" autocomplete='off' data-progress-text="<i class='fa fa-refresh fa-spin'></i>" data-success-text="<i class='fa fa-check'></i>" data-error-text="<i class='fa fa-times'></i>" class="submit btn btn-primary btn-circle btn-xl transition">
                    <i class="fa fa-pencil"></i>
                </button>

            </div>
        </form>
    </div>
</div><?php }} ?>
