jQuery(document).ready(function($){if($.browser.msie){$('body').addClass('MSIE');}
var d=new Date();var clientDate=d.getFullYear()+'-'+(d.getMonth()+ 1)+'-'+d.getDate()+' '+d.getHours()+':'+d.getMinutes();$('input:hidden[name$=ClientHour]').livequery(function(){$(this).val(clientDate);});$('input:hidden[id$=SetClientHour]').livequery(function(){if(d.getHours()!=$(this).val()){$.get(gdn.url('/utility/setclienthour'),{'ClientDate':clientDate,'TransientKey':gdn.definition('TransientKey'),'DeliveryType':'BOOL'});}});$('a.ForgotPassword').live('click',function(){$('.Methods').toggle();$('#Form_User_Password').toggle();$('#Form_User_SignIn').toggle();return false;});if($.fn.autogrow)
$('textarea.Autogrow').livequery(function(){$(this).autogrow();});$.postParseJson=function(json){if(json.Data)json.Data=$.base64Decode(json.Data);return json;}
gdn={};gdn.Libraries={};gdn.definition=function(definition,defaultVal,set){if(defaultVal==null)
defaultVal=definition;var $def=$('#Definitions #'+ definition);var def;if(set){$def.val(defaultVal);def=defaultVal;}else{def=$def.val();if($def.length==0)
def=defaultVal;}
return def;}
gdn.elementSupports=function(element,attribute){var test=document.createElement(element);if(attribute in test)
return true;else
return false;}
gdn.querySep=function(url){return url.indexOf('?')==-1?'?':'&';}
$('li.UserNotifications a span').click(function(){document.location=gdn.url('/profile/notifications');return false;});if($.fn.popup){$('a.Popup').popup();$('a.PopConfirm').popup({'confirm':true,'followConfirm':true});}
$(".PopupWindow").live('click',function(){var $this=$(this);var width=$this.attr('popupWidth');width=width?width:960;var height=$this.attr('popupHeight');height=height?height:600;var left=(screen.width- width)/2;var top=(screen.height- height)/2;var id=$this.attr('id');var href=$this.attr('href');if($this.attr('popupHref'))
href=$this.attr('popupHref');else
href+=gdn.querySep(href)+'display=popup';var win=window.open(href,'Window_'+ id,"left="+left+",top="+top+",width="+width+",height="+height+",status=0,scrollbars=0");if(win)
win.focus();return false;});if($.fn.popup)
$('a.Popdown').popup({hijackForms:false});if($.fn.popup)
$('a.SignInPopup').popup({containerCssClass:'SignInPopup'});$('a.Dismiss').live('click',function(){var anchor=this;var container=$(anchor).parent();var transientKey=gdn.definition('TransientKey');var data='DeliveryType=BOOL&TransientKey='+ transientKey;$.post($(anchor).attr('href'),data,function(response){if(response=='TRUE')
$(container).fadeOut('fast',function(){$(this).remove();});});return false;});if($.fn.handleAjaxForm)
$('.AjaxForm').handleAjaxForm();$('.HoverHelp').hover(function(){$(this).find('.Help').show();},function(){$(this).find('.Help').hide();});var RedirectUrl=gdn.definition('RedirectUrl','');var CheckPopup=gdn.definition('CheckPopup','');if(RedirectUrl!=''){if(CheckPopup&&window.opener){window.opener.location.replace(RedirectUrl);window.close();}else{setTimeout(function(){document.location.replace(RedirectUrl);},200);}}
if($.tableDnD)
$("table.Sortable").tableDnD({onDrop:function(table,row){var tableId=$($.tableDnD.currentTable).attr('id');var transientKey=gdn.definition('TransientKey');var data=$.tableDnD.serialize()+'&DeliveryType=BOOL&TableID='+ tableId+'&TransientKey='+ transientKey;var webRoot=gdn.definition('WebRoot','');$.post(gdn.combinePaths(webRoot,'index.php?p=/dashboard/utility/sort/'),data,function(response){if(response=='TRUE')
$('#'+tableId+' tbody tr td').effect("highlight",{},1000);});}});$('span.Email.EmailUnformatted').livequery(function(){var el=$(this);el.removeClass('EmailUnformatted');var email=$(this).html().replace(/<em[^>]*>dot<\/em>/ig,'.').replace(/<strong[^>]*>at<\/strong>/ig,'@');el.html('<a href="mailto:'+ email+'">'+ email+'</a>');});$.fn.setMaxChars=function(iMaxChars){$(this).live('keyup',function(){var txt=$(this).val();if(txt.length>iMaxChars)
$(this).val(txt.substr(0,iMaxChars));});}
gdn.generateString=function(length){if(length==null)
length=5;var chars='abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789!@#$%*';var string='';var pos=0;for(var i=0;i<length;i++){pos=Math.floor(Math.random()*chars.length);string+=chars.substring(pos,pos+ 1);}
return string;};gdn.combinePaths=function(path1,path2){if(path1.substr(-1,1)=='/')
path1=path1.substr(0,path1.length- 1);if(path2.substring(0,1)=='/')
path2=path2.substring(1);return path1+'/'+ path2;};gdn.processTargets=function(targets){if(!targets||!targets.length)
return;for(i=0;i<targets.length;i++){var item=targets[i];$target=$(item.Target);switch(item.Type){case'Ajax':$.ajax({type:"POST",url:item.Data});break;case'Append':$target.append(item.Data);break;case'Before':$target.before(item.Data);break;case'After':$target.after(item.Data);break;case'Prepend':$target.prepend(item.Data);break;case'Redirect':window.location.replace(item.Data);break;case'Remove':$target.remove();break;case'Text':$target.text(item.Data);break;case'Html':$target.html(item.Data);}}};gdn.requires=function(Library){if(!(Library instanceof Array))
Library=[Library];var Response=true;$(Library).each(function(i,Lib){var LibAvailable=gdn.available(Lib);if(!LibAvailable)Response=false;if(gdn.Libraries[Lib]===false||gdn.Libraries[Lib]===true)
return;gdn.Libraries[Lib]=false;var Src='/js/'+Lib+'.js';var head=document.getElementsByTagName('head')[0];var script=document.createElement('script');script.type='text/javascript';script.src=Src;head.appendChild(script);});if(Response)gdn.loaded(null);return Response;};gdn.loaded=function(Library){if(Library!=null)
gdn.Libraries[Library]=true;$(document).trigger('libraryloaded',[Library])}
gdn.available=function(Library){if(!(Library instanceof Array))
Library=[Library];for(var i=0;i<Library.length;i++){var Lib=Library[i];if(gdn.Libraries[Lib]!==true)return false;}
return true;}
gdn.url=function(path){if(path.indexOf("//")>=0)
return path;var urlFormat=gdn.definition("UrlFormat","");if(path.substr(0,1)=="/")
path=path.substr(1);if(urlFormat.indexOf("?")>=0)
path=path.replace("?","&");return urlFormat.replace("{Path}",path);};if(!gdn.elementSupports('input','placeholder')){$('input:text').each(function(){var $this=$(this);var placeholder=$this.attr('placeholder');if(!$this.val()&&placeholder){$this.val(placeholder);$this.blur(function(){$this.val(placeholder);});$this.focus(function(){if($this.val()==placeholder)
$this.val('');});$this.closest('form').bind('submit',function(){if($this.val()==placeholder)
$this.val('');});}});}
$.fn.popin=function(options){var settings=$.extend({},options);this.each(function(i,elem){var url=$(elem).attr('rel');var $elem=$(elem);$.ajax({url:gdn.url(url),data:{DeliveryType:'VIEW'},success:function(data){$elem.html(data);},complete:function(){$elem.removeClass('Progress TinyProgress');if(settings.complete!=undefined){settings.complete($elem);}}});});};$('.Popin').popin();$.fn.openToggler=function(){$(this).click(function(){var $flyout=$('.Flyout',this);var rel=$(this).attr('rel');if(rel){$(this).attr('rel','');$flyout.addClass('Progress');$.ajax({url:gdn.url(rel),data:{DeliveryType:'VIEW'},success:function(data){$flyout.html(data);},complete:function(){$flyout.removeClass('Progress');}});}
if($flyout.css('display')=='none'){$(this).addClass('Open')
$flyout.show();}else{$flyout.hide()
$(this).removeClass('Open');}});}
$('.ToggleFlyout').openToggler();$('input.SpinOnClick').live('click',function(){$(this).before('<span class="AfterButtonLoading">&#160;</span>').removeClass('SpinOnClick');});$('a.RemoveItem').click(function(){if(!confirm('Are you sure you would like to remove this item?')){return false;}});if(gdn.definition('LocationHash',0)&&window.location.hash==''){window.location.hash=gdn.definition('LocationHash');}
gdn.stats=function(){var StatsURL=gdn.url('settings/analyticstick.json');jQuery.ajax({dataType:'json',type:'post',url:StatsURL,data:{'TransientKey':gdn.definition('TransientKey'),'Path':gdn.definition('Path')},success:function(json){gdn.inform(json);}});}
var AnalyticsTask=gdn.definition('AnalyticsTask',false);if(AnalyticsTask=='tick')
gdn.stats();$('div.InformWrapper.Dismissable a.Close').live('click',function(){$(this).parents('div.InformWrapper').fadeOut('fast',function(){$(this).remove();});});gdn.setAutoDismiss=function(){var timerId=$('div.InformMessages').attr('autodismisstimerid');if(!timerId){timerId=setTimeout(function(){$('div.InformWrapper.AutoDismiss').fadeOut('fast',function(){$(this).remove();});$('div.InformMessages').removeAttr('autodismisstimerid');},5000);$('div.InformMessages').attr('autodismisstimerid',timerId);}}
$('div.InformWrapper.AutoDismiss:first').livequery(function(){gdn.setAutoDismiss();});$('div.InformWrapper').live('mouseover mouseout',function(e){if(e.type=='mouseover'){var timerId=$('div.InformMessages').attr('autodismisstimerid');if(timerId){clearTimeout(timerId);$('div.InformMessages').removeAttr('autodismisstimerid');}}else{gdn.setAutoDismiss();}});gdn.inform=function(response){if(!response||!response.InformMessages)
return;var informMessages=$('div.InformMessages');if(informMessages.length==0){$('<div class="InformMessages"></div>').appendTo('body');informMessages=$('div.InformMessages');}
var wrappers=$('div.InformMessages div.InformWrapper');for(var i=0;i<response.InformMessages.length;i++){css='InformWrapper';if(response.InformMessages[i]['CssClass'])
css+=' '+ response.InformMessages[i]['CssClass'];elementId='';if(response.InformMessages[i]['id'])
elementId=response.InformMessages[i]['id'];sprite='';if(response.InformMessages[i]['Sprite']){css+=' HasSprite';sprite=response.InformMessages[i]['Sprite'];}
dismissCallback=response.InformMessages[i]['DismissCallback'];dismissCallbackUrl=response.InformMessages[i]['DismissCallbackUrl'];if(dismissCallbackUrl)
dismissCallbackUrl=gdn.url(dismissCallbackUrl);try{var message=response.InformMessages[i]['Message'];var emptyMessage=message=='';if(sprite!='')
message='<span class="InformSprite '+sprite+'"></span>'+ message;if(css.indexOf('Dismissable')>0)
message='<a class="Close"><span>×</span></a>'+ message;message='<div class="InformMessage">'+message+'</div>';message=message.replace(/{TransientKey}/g,gdn.definition('TransientKey'));message=message.replace(/{SelfUrl}/g,document.URL);var skip=false;for(var j=0;j<wrappers.length;j++){if($(wrappers[j]).text()==$(message).text()){skip=true;}}
if(!skip){if(elementId!=''){$('#'+elementId).remove();elementId=' id="'+elementId+'"';}
if(!emptyMessage){informMessages.prepend('<div class="'+css+'"'+elementId+'>'+message+'</div>');if(dismissCallback){$('div.InformWrapper:first').find('a.Close').click(eval(dismissCallback));}else if(dismissCallbackUrl){dismissCallbackUrl=dismissCallbackUrl.replace(/{TransientKey}/g,gdn.definition('TransientKey'));var closeAnchor=$('div.InformWrapper:first').find('a.Close');closeAnchor.attr('callbackurl',dismissCallbackUrl);closeAnchor.click(function(){$.ajax({type:"POST",url:$(this).attr('callbackurl'),data:'TransientKey='+gdn.definition('TransientKey'),dataType:'json',error:function(XMLHttpRequest,textStatus,errorThrown){gdn.informMessage(XMLHttpRequest.responseText,'Dismissable AjaxError');},success:function(json){gdn.inform(json);}});});}}}}catch(e){}}
informMessages.show();}
gdn.informMessage=function(message,options){if(!options)
options=new Array();if(typeof(options)=='string'){var css=options;options=new Array();options['CssClass']=css;}
options['Message']=message;if(!options['CssClass'])
options['CssClass']='Dismissable AutoDismiss';gdn.inform({'InformMessages':new Array(options)});}
var informMessageStack=gdn.definition('InformMessageStack',false);if(informMessageStack){informMessageStack={'InformMessages':eval($.base64Decode(informMessageStack))};gdn.inform(informMessageStack);}
pingForNotifications=function(wait){}
if(gdn.definition('SignedIn','0')!='0')
pingForNotifications(false);stash=function(name,value){$.ajax({type:"POST",url:gdn.url('session/stash'),data:{'TransientKey':gdn.definition('TransientKey'),'Name':name,'Value':value},dataType:'json',error:function(XMLHttpRequest,textStatus,errorThrown){gdn.informMessage(XMLHttpRequest.responseText,'Dismissable AjaxError');},success:function(json){gdn.inform(json);return json.Unstash;}});return'';}
$('a.Stash').click(function(){var comment=$('#Form_Comment textarea').val();if(comment!='')
stash('CommentForDiscussionID_'+ gdn.definition('DiscussionID'),comment);});});(function($){var keyString="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";var uTF8Encode=function(string){string=string.replace(/\x0d\x0a/g,"\x0a");var output="";for(var n=0;n<string.length;n++){var c=string.charCodeAt(n);if(c<128){output+=String.fromCharCode(c);}else if((c>127)&&(c<2048)){output+=String.fromCharCode((c>>6)|192);output+=String.fromCharCode((c&63)|128);}else{output+=String.fromCharCode((c>>12)|224);output+=String.fromCharCode(((c>>6)&63)|128);output+=String.fromCharCode((c&63)|128);}}
return output;};var uTF8Decode=function(input){var string="";var i=0;var c=c1=c2=0;while(i<input.length){c=input.charCodeAt(i);if(c<128){string+=String.fromCharCode(c);i++;}else if((c>191)&&(c<224)){c2=input.charCodeAt(i+1);string+=String.fromCharCode(((c&31)<<6)|(c2&63));i+=2;}else{c2=input.charCodeAt(i+1);c3=input.charCodeAt(i+2);string+=String.fromCharCode(((c&15)<<12)|((c2&63)<<6)|(c3&63));i+=3;}}
return string;}
$.extend({base64Encode:function(input){var output="";var chr1,chr2,chr3,enc1,enc2,enc3,enc4;var i=0;input=uTF8Encode(input);while(i<input.length){chr1=input.charCodeAt(i++);chr2=input.charCodeAt(i++);chr3=input.charCodeAt(i++);enc1=chr1>>2;enc2=((chr1&3)<<4)|(chr2>>4);enc3=((chr2&15)<<2)|(chr3>>6);enc4=chr3&63;if(isNaN(chr2)){enc3=enc4=64;}else if(isNaN(chr3)){enc4=64;}
output=output+ keyString.charAt(enc1)+ keyString.charAt(enc2)+ keyString.charAt(enc3)+ keyString.charAt(enc4);}
return output;},base64Decode:function(input){var output="";var chr1,chr2,chr3;var enc1,enc2,enc3,enc4;var i=0;input=input.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(i<input.length){enc1=keyString.indexOf(input.charAt(i++));enc2=keyString.indexOf(input.charAt(i++));enc3=keyString.indexOf(input.charAt(i++));enc4=keyString.indexOf(input.charAt(i++));chr1=(enc1<<2)|(enc2>>4);chr2=((enc2&15)<<4)|(enc3>>2);chr3=((enc3&3)<<6)|enc4;output=output+ String.fromCharCode(chr1);if(enc3!=64){output=output+ String.fromCharCode(chr2);}
if(enc4!=64){output=output+ String.fromCharCode(chr3);}}
output=uTF8Decode(output);return output;}});})(jQuery);jQuery(window).load(function(){var toggler=function(t_img,t_width){if(t_img.css('width')=='auto')
t_img.css('width',t_width);else
t_img.css('width','auto');return false;}
jQuery('div.Message img').each(function(i,img){var img=jQuery(img);var container=img.parents('div.Message');if(img.width()>container.width()){var smwidth=container.width();img.css('width',smwidth).css('cursor','pointer');img.after('<div class="ImageResized">'+ gdn.definition('ImageResized','This image has been resized to fit in the page. Click to enlarge.')+'</div>');img.next().click(function(){return toggler(img,smwidth);});img.click(function(){return toggler(img,smwidth);})}});});