jQuery(document).ready(function(jQuery){var self=null;jQuery.fn.autogrow=function(o)
{return this.each(function(){new jQuery.autogrow(this,o);});};jQuery.autogrow=function(e,o)
{this.options=o||{};this.dummy=null;this.interval=null;this.line_height=this.options.lineHeight||parseInt(jQuery(e).css('line-height'));this.min_height=this.options.minHeight||parseInt(jQuery(e).css('min-height'));this.max_height=this.options.maxHeight||parseInt(jQuery(e).css('max-height'));;this.textarea=jQuery(e);if(this.line_height==NaN)
this.line_height=0;this.init();};jQuery.autogrow.fn=jQuery.autogrow.prototype={autogrow:'1.2.2'};jQuery.autogrow.fn.extend=jQuery.autogrow.extend=jQuery.extend;jQuery.autogrow.fn.extend({init:function(){var self=this;this.textarea.css({overflow:'hidden',display:'block'});this.textarea.bind('focus',function(){self.startExpand()}).bind('blur',function(){self.stopExpand()});this.checkExpand();},startExpand:function(){var self=this;this.interval=window.setInterval(function(){self.checkExpand()},400);},stopExpand:function(){clearInterval(this.interval);},checkExpand:function(){var current_chars=this.textarea.val().length;if(this.last_chars==undefined)this.last_chars=current_chars;if(this.char_diff==undefined)this.cumulative_char_diff=0;var iteration_char_difference=current_chars- this.last_chars;this.last_chars=current_chars;this.cumulative_char_diff+=iteration_char_difference;var absolute_char_diff=Math.abs(this.char_diff);var height_difference=Math.abs(this.textarea.attr('scrollHeight')- this.textarea.attr('clientHeight'));if(!height_difference&&absolute_char_diff<10)return;if(this.dummy==null)
{this.dummy=jQuery('<div></div>');this.dummy.css({'font-size':this.textarea.css('font-size'),'font-family':this.textarea.css('font-family'),'width':this.textarea.css('width'),'padding-left':this.textarea.css('padding-left'),'padding-right':this.textarea.css('padding-right'),'padding-top':this.textarea.css('padding-top'),'padding-bottom':this.textarea.css('padding-bottom'),'line-height':this.line_height+'px','overflow-x':'hidden','position':'absolute','top':0,'left':-9999}).appendTo('body');}
var html=this.textarea.val().replace(/(<|>)/g,'');if(jQuery.browser.msie)
{html=html.replace(/\n/g,'<BR>new');}
else
{html=html.replace(/\n/g,'<br>new');}
if(this.dummy.html()!=html)
{this.dummy.html(html);if(this.max_height>0&&(this.dummy.height()+ this.line_height>this.max_height))
{this.textarea.css('overflow-y','auto');}
else
{this.textarea.css('overflow-y','hidden');if(this.textarea.height()<this.dummy.height()+ this.line_height||(this.dummy.height()<this.textarea.height()))
{this.textarea.css({height:(this.dummy.height()+ this.line_height)+'px'});}}}
this.cumulative_char_diff=0;}});});