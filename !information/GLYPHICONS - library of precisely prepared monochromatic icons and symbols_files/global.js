(function($) {

    /*!
     *
     *  Project:  GLYPHICONS
     *  Author:   Petr Urbanek - www.r4ms3s.cz
     *  Twitter:  @r4ms3scz
     *
     * @param {Object} window, document, undefined
     *
     */

     (function(window, document, undefined) {
        
        // Defaults
        // =====================================
       
        var r4 = window.r4 = {
            utils : {},
            cache : {}
        };
      

        // Methods
        // =====================================

        r4.utils.init = function() {
            r4.cache.window                = $(window);
            r4.cache.document              = $(document);
            r4.cache.html                  = $('html');
            r4.cache.body                  = $('body');

            r4.cache.header                = r4.cache.body.find('header');
            r4.cache.footer                = r4.cache.body.find('footer');
            r4.cache.section               = r4.cache.body.find('section');
        };


        // GLYPHICONS
        r4.utils.glyphiconsico = function(){

            var content = $('.font-list-glyphicons'),
                list = content.find('li'),
                box = content.find('.glyphicons-box');

            // LIST ICONS
            list.each(function(){
                var li = $(this),
                    a = li.find('a');

                li.on('mouseenter mouseleave click', function(e){
                    e.preventDefault();

                    var left = a.offset().left - content.offset().left - 3,
                        top = a.offset().top - content.offset().top - 5,
                        name = a.data('name'),
                        type = a.data('type'),
                        prefix = a.data('prefix'),
                        utf = a.data('utf');

                    if(e.type === 'mouseenter' || e.type === 'click'){
                        list.removeClass('act');
                        box.addClass('act');

                        li.addClass('act');
                        box.find('strong').html(name);
                        box.find('.prefix').html(prefix);
                        box.find('.red').html(name);
                        box.find('.utf').html(utf);

                        box.css({
                            left: left + 'px',
                            top: top + 'px'
                        });
                        box.stop().fadeTo(200, 1);
                    }else{
                        box.stop().fadeTo(200, 0, function(){
                            list.removeClass('act');
                            box.removeClass('act');
                        });
                    }
                });
            });

            // SELECT
            $('.font-list-glyphicons select').select2();
            $('.font-list-glyphicons select').on('change', function(e){
                var val = $('.font-list-glyphicons select').val();

                if(val == 'new'){
                    list.addClass('hide');
                    list.find('a[data-type*="NEW IN 1.8"]').parent().removeClass('hide');
                }else{
                    list.removeClass('hide');
                }
            });
            
            // SEARCH INPUT
            $('.glyphicons-input').on('keyup', function(){
                var input = $(this),
                    val = input.val(),
                    select = $('.font-list-glyphicons select').val();

                if(val.length >= 1){
                    list.addClass('hide');

                    if(select == 'new'){
                        list.find('a[data-name*="' + val + '"]').parent().removeClass('hide');
                        list.find('a[data-utf*="' + val + '"]').parent().removeClass('hide');

                        list.find('a').not('[data-type*="NEW IN 1.8"]').parent().addClass('hide');
                    }else{
                        list.find('a[data-name*="' + val + '"]').parent().removeClass('hide');
                        list.find('a[data-utf*="' + val + '"]').parent().removeClass('hide');
                    }
                }else{
                    if(select == 'new'){
                        list.addClass('hide');
                        list.find('a[data-type*="NEW IN 1.8"]').parent().removeClass('hide');
                    }else{
                        list.removeClass('hide');
                    }
                }
            });
        }

        // HALFLINGS
        r4.utils.halflingsico = function(){

            var content = $('.font-list-halflings'),
                list = content.find('li'),
                box = content.find('.halflings-box');

            // LIST ICONS
            list.each(function(){
                var li = $(this),
                    a = li.find('a');

                li.on('mouseenter mouseleave click', function(e){
                    e.preventDefault();

                    var left = a.offset().left - content.offset().left - 6,
                        top = a.offset().top - content.offset().top - 7,
                        name = a.data('name'),
                        type = a.data('type'),
                        prefix = a.data('prefix'),
                        utf = a.data('utf');

                    if(e.type === 'mouseenter' || e.type === 'click'){
                        list.removeClass('act');
                        box.addClass('act');

                        li.addClass('act');
                        box.find('strong').html(name);
                        box.find('.prefix').html(prefix);
                        box.find('.red').html(name);
                        box.find('.utf').html(utf);

                        box.css({
                            left: left + 'px',
                            top: top + 'px'
                        });
                        box.stop().fadeTo(200, 1);
                    }else{
                        box.stop().fadeTo(200, 0, function(){
                            list.removeClass('act');
                            box.removeClass('act');
                        });
                    }
                });
            });

            // SELECT
            $('.font-list-halflings select').select2();
            $('.font-list-halflings select').on('change', function(e){
                var val = $('.font-list-halflings select').val();

                if(val == 'new'){
                    list.addClass('hide');
                    list.find('a[data-type*="NEW IN 1.8"]').parent().removeClass('hide');
                }else{
                    list.removeClass('hide');
                }
            });
            
            // SEARCH INPUT
            $('.halflings-input').on('keyup', function(){
                var input = $(this),
                    val = input.val(),
                    select = $('.font-list-halflings select').val();

                if(val.length >= 1){
                    list.addClass('hide');

                    if(select == 'new'){
                        list.find('a[data-name*="' + val + '"]').parent().removeClass('hide');
                        list.find('a[data-utf*="' + val + '"]').parent().removeClass('hide');

                        list.find('a').not('[data-type*="NEW IN 1.8"]').parent().addClass('hide');
                    }else{
                        list.find('a[data-name*="' + val + '"]').parent().removeClass('hide');
                        list.find('a[data-utf*="' + val + '"]').parent().removeClass('hide');
                    }
                }else{
                    if(select == 'new'){
                        list.addClass('hide');
                        list.find('a[data-type*="NEW IN 1.8"]').parent().removeClass('hide');
                    }else{
                        list.removeClass('hide');
                    }
                }
            });
        }

        // SOCIAL
        r4.utils.socialico = function(){

            var content = $('.font-list-social'),
                list = content.find('li'),
                box = content.find('.social-box');

            // LIST ICONS
            list.each(function(){
                var li = $(this),
                    a = li.find('a');

                li.on('mouseenter mouseleave click', function(e){
                    e.preventDefault();

                    var left = a.offset().left - content.offset().left - 3,
                        top = a.offset().top - content.offset().top - 6,
                        name = a.data('name'),
                        type = a.data('type'),
                        prefix = a.data('prefix'),
                        utf = a.data('utf');

                    if(e.type === 'mouseenter' || e.type === 'click'){
                        list.removeClass('act');
                        box.addClass('act');

                        li.addClass('act');
                        box.find('strong').html(name);
                        box.find('.prefix').html(prefix);
                        box.find('.red').html(name);
                        box.find('.utf').html(utf);

                        box.css({
                            left: left + 'px',
                            top: top + 'px'
                        });
                        box.stop().fadeTo(200, 1);
                    }else{
                        box.stop().fadeTo(200, 0, function(){
                            list.removeClass('act');
                            box.removeClass('act');
                        });
                    }
                });
            });

            // SELECT
            $('.font-list-social select').select2();
            $('.font-list-social select').on('change', function(e){
                var val = $('.font-list-social select').val();

                if(val == 'new'){
                    list.addClass('hide');
                    list.find('a[data-type*="NEW IN 1.8"]').parent().removeClass('hide');
                }else{
                    list.removeClass('hide');
                }
            });
            
            // SEARCH INPUT
            $('.social-input').on('keyup', function(){
                var input = $(this),
                    val = input.val(),
                    select = $('.font-list-social select').val();

                if(val.length >= 1){
                    list.addClass('hide');

                    if(select == 'new'){
                        list.find('a[data-name*="' + val + '"]').parent().removeClass('hide');
                        list.find('a[data-utf*="' + val + '"]').parent().removeClass('hide');

                        list.find('a').not('[data-type*="NEW IN 1.8"]').parent().addClass('hide');
                    }else{
                        list.find('a[data-name*="' + val + '"]').parent().removeClass('hide');
                        list.find('a[data-utf*="' + val + '"]').parent().removeClass('hide');
                    }
                }else{
                    if(select == 'new'){
                        list.addClass('hide');
                        list.find('a[data-type*="NEW IN 1.8"]').parent().removeClass('hide');
                    }else{
                        list.removeClass('hide');
                    }
                }
            });
        }

        // FILETYPES
        r4.utils.filetypesico = function(){

            var content = $('.font-list-filetypes'),
                list = content.find('li'),
                box = content.find('.filetypes-box');

            // LIST ICONS
            list.each(function(){
                var li = $(this),
                    a = li.find('a');

                li.on('mouseenter mouseleave click', function(e){
                    e.preventDefault();

                    var left = a.offset().left - content.offset().left - 3,
                        top = a.offset().top - content.offset().top - 6,
                        name = a.data('name'),
                        type = a.data('type'),
                        prefix = a.data('prefix'),
                        utf = a.data('utf');

                    if(e.type === 'mouseenter' || e.type === 'click'){
                        list.removeClass('act');
                        box.addClass('act');

                        li.addClass('act');
                        box.find('strong').html(name);
                        box.find('.prefix').html(prefix);
                        box.find('.red').html(name);
                        box.find('.utf').html(utf);

                        box.css({
                            left: left + 'px',
                            top: top + 'px'
                        });
                        box.stop().fadeTo(200, 1);
                    }else{
                        box.stop().fadeTo(200, 0, function(){
                            list.removeClass('act');
                            box.removeClass('act');
                        });
                    }
                });
            });

            // SELECT
            $('.font-list-filetypes select').select2();
            $('.font-list-filetypes select').on('change', function(e){
                var val = $('.font-list-filetypes select').val();

                if(val !== 'ALL GLYPHICONS'){
                    list.addClass('hide');
                    list.find('a[data-type*="' + val + '"]').parent().removeClass('hide');
                }else{
                    list.removeClass('hide');
                }
            });
            
            // SEARCH INPUT
            $('.filetypes-input').on('keyup', function(){
                var input = $(this),
                    val = input.val(),
                    select = $('.font-list-filetypes select').val();

                if(select !== 'ALL GLYPHICONS'){
                    list.addClass('hide');

                    if(val.length >= 1){
                        list.find('a[data-name*="' + val + '"]').parent().removeClass('hide');
                        list.find('a[data-utf*="' + val + '"]').parent().removeClass('hide');

                        list.find('a').not('[data-type*="' + select + '"]').parent().addClass('hide');
                    }else{
                        list.find('a[data-name*="' + val + '"]').parent().removeClass('hide');
                        list.find('a[data-utf*="' + val + '"]').parent().removeClass('hide');

                        list.find('a[data-type*="' + select + '"]').parent().removeClass('hide');
                    }
                }else{
                    if(val.length >= 1){
                        list.addClass('hide');
                        list.find('a[data-name*="' + val + '"]').parent().removeClass('hide');
                    }else{
                        list.removeClass('hide');
                    }
                }
            });
        }


        r4.utils.domLoad = function() {
            if($('.content-font-list'.length)){

                setTimeout(function(){ 
                    $('.glyphicons-badge').fadeTo('normal', 0.9);
                }, 500);

                $('.tabs a').on('click', function(e){
                    e.preventDefault();

                    $('.font-list-glyphicons select').select2('val', 'ALL GLYPHICONS');
                    $('.font-list-halflings select').select2('val', 'ALL GLYPHICONS');
                    $('.font-list-social select').select2('val', 'ALL GLYPHICONS');
                    $('.font-list-filetypes select').select2('val', 'ALL GLYPHICONS');

                    $('.font-list li').removeClass('act').removeClass('hide');

                    var a = $(this).attr('rel');

                    $('.tabs li').removeClass('act');
                    $(this).parents('li').addClass('act');

                    $('.font-list').removeClass('act');
                    $('.' + a).addClass('act');

                    $('.font-list-badge').attr('style', '').removeAttr('style');
                    setTimeout(function(){ 
                        $('.' + a).find('.font-list-badge').fadeTo('normal', 0.9);
                    }, 500);

                })

                r4.utils.glyphiconsico();
                r4.utils.halflingsico();
                r4.utils.socialico();
                r4.utils.filetypesico();
            }

            // Add new badge to main menu
            $('#menu-menu .examples a').append('<span class="badge">NEW</span>');
        };


        // Initialize Events
        // =====================================

        r4.utils.init();

        jQuery(function($) {
            r4.utils.domLoad();
        });


    })(window, document);

})(jQuery)