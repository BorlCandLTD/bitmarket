if (!window.KAMENT) window.KAMENT = {};
KAMENT._rootId = "kament_comments";
KAMENT._domain = "svkament.ru";
KAMENT._popupWnd = "kament_popupWnd";
KAMENT.main_iframe = undefined; // comments iframe
KAMENT.mouse_x = 0;
KAMENT.mouse_y = 0;

KAMENT.old_ie = (function(){
    var div = document.createElement('div');
	div.innerHTML = '<!--[if lt IE 8]><i></i><![endif]-->';
	var all = div.getElementsByTagName('i');
	return all.length > 0 ? true : false;
}());

KAMENT.addEvent = function(element, evnt, funct){
  if (element.attachEvent)
   return element.attachEvent('on'+evnt, funct);
  else
   return element.addEventListener(evnt, funct, false);
};


KAMENT.hasClass = function(ele, cls)
{
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
};

KAMENT.addClass = function(ele, cls)
{
	if(!KAMENT.hasClass(ele,cls)) {
		ele.className = ele.className + " " + cls;
	}
};

KAMENT.removeClass = function(ele,cls)
{
	if (KAMENT.hasClass(ele,cls)) {
		var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		ele.className=ele.className.replace(reg,' ');
	}
};

KAMENT.Error = function(error_msg) {
	console.error('[KAMENT]error: ' + error_msg);
};

KAMENT.getMetaValue = function(name) {
   var metas = document.getElementsByTagName('meta'); 

   for (i=0; i<metas.length; i++) { 
      if (metas[i].getAttribute("property") == name) { 
         return metas[i].getAttribute("content"); 
      } 
   } 

	return "";
};



KAMENT.isBrowserOk = function() {
	return !KAMENT.old_ie;
}

KAMENT.getOffset = function(el) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft;
        _y += el.offsetTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}

KAMENT.colorGetter = {
	'colorNames':  {
        aliceblue: 'f0f8ff',
        antiquewhite: 'faebd7',
        aqua: '00ffff',
        aquamarine: '7fffd4',
        azure: 'f0ffff',
        beige: 'f5f5dc',
        bisque: 'ffe4c4',
        black: '000000',
        blanchedalmond: 'ffebcd',
        blue: '0000ff',
        blueviolet: '8a2be2',
        brown: 'a52a2a',
        burlywood: 'deb887',
        cadetblue: '5f9ea0',
        chartreuse: '7fff00',
        chocolate: 'd2691e',
        coral: 'ff7f50',
        cornflowerblue: '6495ed',
        cornsilk: 'fff8dc',
        crimson: 'dc143c',
        cyan: '00ffff',
        darkblue: '00008b',
        darkcyan: '008b8b',
        darkgoldenrod: 'b8860b',
        darkgray: 'a9a9a9',
        darkgreen: '006400',
        darkkhaki: 'bdb76b',
        darkmagenta: '8b008b',
        darkolivegreen: '556b2f',
        darkorange: 'ff8c00',
        darkorchid: '9932cc',
        darkred: '8b0000',
        darksalmon: 'e9967a',
        darkseagreen: '8fbc8f',
        darkslateblue: '483d8b',
        darkslategray: '2f4f4f',
        darkturquoise: '00ced1',
        darkviolet: '9400d3',
        deeppink: 'ff1493',
        deepskyblue: '00bfff',
        dimgray: '696969',
        dodgerblue: '1e90ff',
        feldspar: 'd19275',
        firebrick: 'b22222',
        floralwhite: 'fffaf0',
        forestgreen: '228b22',
        fuchsia: 'ff00ff',
        gainsboro: 'dcdcdc',
        ghostwhite: 'f8f8ff',
        gold: 'ffd700',
        goldenrod: 'daa520',
        gray: '808080',
        green: '008000',
        greenyellow: 'adff2f',
        honeydew: 'f0fff0',
        hotpink: 'ff69b4',
        indianred : 'cd5c5c',
        indigo : '4b0082',
        ivory: 'fffff0',
        khaki: 'f0e68c',
        lavender: 'e6e6fa',
        lavenderblush: 'fff0f5',
        lawngreen: '7cfc00',
        lemonchiffon: 'fffacd',
        lightblue: 'add8e6',
        lightcoral: 'f08080',
        lightcyan: 'e0ffff',
        lightgoldenrodyellow: 'fafad2',
        lightgrey: 'd3d3d3',
        lightgreen: '90ee90',
        lightpink: 'ffb6c1',
        lightsalmon: 'ffa07a',
        lightseagreen: '20b2aa',
        lightskyblue: '87cefa',
        lightslateblue: '8470ff',
        lightslategray: '778899',
        lightsteelblue: 'b0c4de',
        lightyellow: 'ffffe0',
        lime: '00ff00',
        limegreen: '32cd32',
        linen: 'faf0e6',
        magenta: 'ff00ff',
        maroon: '800000',
        mediumaquamarine: '66cdaa',
        mediumblue: '0000cd',
        mediumorchid: 'ba55d3',
        mediumpurple: '9370d8',
        mediumseagreen: '3cb371',
        mediumslateblue: '7b68ee',
        mediumspringgreen: '00fa9a',
        mediumturquoise: '48d1cc',
        mediumvioletred: 'c71585',
        midnightblue: '191970',
        mintcream: 'f5fffa',
        mistyrose: 'ffe4e1',
        moccasin: 'ffe4b5',
        navajowhite: 'ffdead',
        navy: '000080',
        oldlace: 'fdf5e6',
        olive: '808000',
        olivedrab: '6b8e23',
        orange: 'ffa500',
        orangered: 'ff4500',
        orchid: 'da70d6',
        palegoldenrod: 'eee8aa',
        palegreen: '98fb98',
        paleturquoise: 'afeeee',
        palevioletred: 'd87093',
        papayawhip: 'ffefd5',
        peachpuff: 'ffdab9',
        peru: 'cd853f',
        pink: 'ffc0cb',
        plum: 'dda0dd',
        powderblue: 'b0e0e6',
        purple: '800080',
        red: 'ff0000',
        rosybrown: 'bc8f8f',
        royalblue: '4169e1',
        saddlebrown: '8b4513',
        salmon: 'fa8072',
        sandybrown: 'f4a460',
        seagreen: '2e8b57',
        seashell: 'fff5ee',
        sienna: 'a0522d',
        silver: 'c0c0c0',
        skyblue: '87ceeb',
        slateblue: '6a5acd',
        slategray: '708090',
        snow: 'fffafa',
        springgreen: '00ff7f',
        steelblue: '4682b4',
        tan: 'd2b48c',
        teal: '008080',
        thistle: 'd8bfd8',
        tomato: 'ff6347',
        turquoise: '40e0d0',
        violet: 'ee82ee',
        violetred: 'd02090',
        wheat: 'f5deb3',
        white: 'ffffff',
        whitesmoke: 'f5f5f5',
        yellow: 'ffff00',
        yellowgreen: '9acd32'
    },

    'parseSingle': function(s) {
        s = s + s;
        return(parseInt(s, 16));
    },

	'prepareResult': function(color) {

		color = color.replace(/ /g, "").toLowerCase();
		if (this.colorNames[color])
		{
			color = "#" + this.colorNames[color];
		}
		var r = 256, g = 256, b = 256;
		if (color.indexOf("#") == 0)
		{
			color = color.slice(1);
			if (color.length == 3)
			{
				r = this.parseSingle(color.slice(0,1));
				g = this.parseSingle(color.slice(1,2));
				b = this.parseSingle(color.slice(2,3));
			}
			else if (color.length == 6)
			{
				r = parseInt(color.slice(0,2), 16);
				g = parseInt(color.slice(2,4), 16);
				b = parseInt(color.slice(4,6), 16);
			}
		}
		else if (color.indexOf("rgb") == 0)
		{
			var results = color.match(/^rgba?\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})/);
			if (results && results.length >= 4)
			{
				r = parseInt(results[1], 10);
				g = parseInt(results[2], 10);
				b = parseInt(results[3], 10);
			}
		}
		var luminance = (0.3 * r + 0.59 * g + 0.11 * b) / 256;
		return({r: r, g: g, b: b, luminance: luminance});
	},

	'getColor': function(o) {
		var color;
		if(window.getComputedStyle) {
			color = window.getComputedStyle(o,null);
			color = color.getPropertyValue("color");
		}
		else if(o.currentStyle)
			color = o.currentStyle.color;

		return this.prepareResult(color);
	},



	'getBackgroundColor': function(o) {
		var color;
		while (o)
		{
		   // color = YD.getComputedStyle(o, "backgroundColor");

			if(window.getComputedStyle) {
				color = window.getComputedStyle(o,null);
				color = color.getPropertyValue("background-color");
			}
			else if(o.currentStyle)
				color = o.currentStyle.backgroundColor;

			if (color && color != "transparent" && color != "rgba(0, 0, 0, 0)")
			{
				break;
			}
			if (o == document.body)
			{
				color = "#ffffff";
				break;
			}
			o = o.parentNode;
		}

		return this.prepareResult(color);
	}
}

KAMENT.darkTheme = function() {
	var test_p = document.createElement('P'); 
	var root = document.getElementById(this._rootId);
	root.appendChild(test_p);

	color = KAMENT.colorGetter.getBackgroundColor(test_p);
	test_p.parentNode.removeChild(test_p);

	if(!color)
		return false;

	if(color.r > 128 || color.g > 128 || color.b > 128)
		return false;
	else
		return true;
}

KAMENT.linkColor = function()
{
	function componentToHex(c) {
		var hex = c.toString(16);
		return hex.length == 1 ? "0" + hex : hex;
	}

	var test_a = document.createElement('a'); 
	test_a.setAttribute('href', '#');
	var root = document.getElementById(this._rootId);
	root.appendChild(test_a);

	color = KAMENT.colorGetter.getColor(test_a);

	test_a.parentNode.removeChild(test_a);

	if(!color)
		return '';

	return '#' + componentToHex( color.r )+ componentToHex( color.g ) + componentToHex( color.b );
}

KAMENT.iframeLoaded = function() {
	var hash = window.location.hash;
	if(hash.match(/^#kament_comments(\/(.*))*$/)) {
		KAMENT.main_iframe.scrollIntoView();
	}
}

KAMENT.Init = function() {
	if(!window.kament_subdomain) {
		KAMENT.Error("Subdomain name not set");
		return;
	}

	if(!document.getElementById(this._rootId)) {
		return;
	}
	// clear possible plain-text content
	document.getElementById(this._rootId).innerHTML = '';


	if(!window.pm)
	this.attachScript('http://' + KAMENT._domain + "/js/libs/postmessage.js");

		var css=document.createElement("link")
		css.setAttribute("rel", "stylesheet")
		css.setAttribute("type", "text/css")
		css.setAttribute("href", 'http://' + kament_subdomain + '.' + KAMENT._domain + '/css/embed.css')

		var head = document.getElementsByTagName("head")[0];
		head.appendChild(css);


		var useDarkTheme = KAMENT.darkTheme();
		var linkColor = KAMENT.linkColor();

		var url_params = {};
		if(KAMENT._view)
			url_params['view'] = KAMENT._view;
		if(KAMENT._filter_type)
			url_params['filter_type'] = KAMENT._filter_type;
		if(useDarkTheme)
			url_params['dark'] = 1;

		url_params['linkcolor'] = linkColor;

		var hash = window.location.hash;

		var hash_regexp = /^#kament_comments(\/(.*))*$/;
		var match = hash_regexp.exec(hash);
		if(match && match[2])
			url_params['hash'] = match[2];

		var current_url;
		var page_title;

		if(window.kament_page_url != undefined)
			current_url = window.kament_page_url;
		else
			current_url = this.getCurrentURL();

		if(window.kament_page_title)
			page_title = window.kament_page_title;
		else
			page_title = this.getCurrentTitle();

		var page_description = this.getMetaValue('og:description');



		var page_image = this.getMetaValue('og:image');

		if(page_image) {
			// not a full url
			if(!page_image.match(/^https*:\/\/.*/)) {

				// not a //example.com url ?
				if(!page_image.match(/^\/\/.*/)) {

					// check if starts with current domain
					if(page_image.indexOf(window.location.host) != 0 ) {
						if(page_image.indexOf('/') != 0)
							page_image = '/' + page_image;

						page_image = window.location.host + page_image;
					} // add domain
					page_image = '//' + page_image;

				}

				page_image = window.location.protocol + page_image;
			} // make full url
		}

		url_params['description'] = page_description;
		url_params['image'] = page_image;

		var iframe_url = this.buildIframeUrl(current_url, page_title, url_params);

		var root = document.getElementById(this._rootId);
		this.main_iframe = document.createElement('IFRAME'); 
		this.main_iframe.setAttribute('src', iframe_url); 
		this.main_iframe.setAttribute('id', 'kament_comments_root'); 
		this.main_iframe.setAttribute('name', 'kament_comments_root'); 
		this.main_iframe.setAttribute('style', 'border:none;'); 
		this.main_iframe.setAttribute('scrolling', 'no'); 
		this.main_iframe.setAttribute('frameBorder', '0'); 

		this.main_iframe.style.width = '100%';
		this.main_iframe.style.height = '0px';

		this.main_iframe.onload = function(e) {
			KAMENT.FrameManager.registerFrame(this)
			KAMENT.FrameManager.init();
			KAMENT.updateViewport();
			setTimeout(KAMENT.iframeLoaded, 300);

			//window.onscroll = KAMENT.updateViewport;
			//window.onresize = KAMENT.updateViewport;
			//document.onmousemove = KAMENT.mouseMoveCallback; //use document. {window} fires in other tabs, lol

			KAMENT.addEvent( window, 'scroll', KAMENT.updateViewport);
			KAMENT.addEvent( window, 'resize', KAMENT.updateViewport);
			KAMENT.addEvent( document, 'mousemove', KAMENT.mouseMoveCallback);
			KAMENT.addEvent( document, 'keydown', KAMENT.onKeyDown);
		};

		root.appendChild(this.main_iframe);
		this.main_iframe.focus();

		// update viewport on resize (callbacks do not work here)
		setInterval(function() {
			var w = root.offsetWidth;
			if(!this.old_w)
				this.old_w = 0;

			if(w !== this.old_w) {
				this.old_w = w;
				KAMENT.updateViewport();
			}

		}, 1000);

		// attach onKeyDown event for modals

		//if(kament_subdomain == 'vestiru-sport') {
		////if(kament_subdomain == 'test') {
			////KAMENT.VestiRuInit();
		//}


};

//KAMENT.VestiRuInit = function()
//{
	//var re = /\/(\d+)$/;
	//var found = window.location.href.match(re);
	//if(!found) {
		//console.error('live_id not found in url');
		//return;
	//}
	//var live_id = found[1];

	//var startTimeArray = {
		//123: '2014-01-06T20:03:00+04:00'
	 //,880: '2014-01-08T17:20:00+04:00'
	 //,881: '2014-01-09T17:20:00+04:00'
	 //,900: '2014-01-10T17:05:00+04:00'
	 //,901: '2014-01-10T15:35:00+04:00'
	 //,920: '2014-01-12T14:05:00+04:00'
	 //,921: '2014-01-12T16:05:00+04:00'

	 //,940: '2014-01-16T17:20:00+04:00'
	 //,960: '2014-01-17T17:40:00+04:00'
	 //,980: '2014-01-18T14:35:00+04:00'
	 //,981: '2014-01-18T17:20:00+04:00'
	 //,961: '2014-01-19T14:05:00+04:00'
	 //,962: '2014-01-19T17:05:00+04:00'
	//};

	//if(typeof startTimeArray[live_id] == 'undefined') {
		//console.error('Event start time not found');
		//return;
	//}

	//window.SVGameMasterPayload = {
		 //'subdomain': 'vestiru-sport'
		//,'visit_activity': 'broadcastVisit'
		//,'visit_start': startTimeArray[live_id]
		//,'visit_duration': '1800'
		//,'metadata': {
			//'category': 'biathlon'
			//,'live_id': live_id
		//}
		//,'sig': ''
	//};

	//var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true;
	//node.src = 'http://' + SVGameMasterPayload['subdomain'] + '.' + KAMENT._domain + '/js/SVGameMaster.js';
	//(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(node);
//}

KAMENT.updateMouseXY = function(e)
{ 
  if (!e) e = window.event; // IE workaround
  if (e)
  { 
    if (e.pageX || e.pageY) { 
      KAMENT.mouse_x = e.pageX;
      KAMENT.mouse_y = e.pageY;
    }
    else if (e.clientX || e.clientY) { 
      KAMENT.mouse_x = e.clientX + document.body.scrollLeft;
      KAMENT.mouse_y = e.clientY + document.body.scrollTop;
    }  
  }
}


KAMENT.sendHeartbeat = function() {
	if(KAMENT.alive) {
		pm({
		  target: window.frames[KAMENT.main_iframe.id],
		  type:   "alive", 
		  url: KAMENT.main_iframe.contentWindow.location
		});

		KAMENT.alive = 0;
	}
}

KAMENT.mouseMoveCallback = function() {
	var old_x = KAMENT.mouse_x, old_y = KAMENT.mouse_y;
	KAMENT.updateMouseXY();

	if(old_x != KAMENT.mouse_x || old_y != KAMENT.mouse_y) {
		KAMENT.alive = 1;
		//console.log('move: ' + KAMENT.mouse_x + ', ' + KAMENT.mouse_y);
	}
}

KAMENT.updateViewport = function() {

	var viewport_height = window.innerHeight;
	var ifrm_top = KAMENT.main_iframe.getBoundingClientRect()['top']; //distance from viewport_top to element top. 
	var ifrm_bottom = -1;

	if(ifrm_top < 0) { // iframe top is above viewport
		ifrm_top = -1*ifrm_top;
		ifrm_bottom = ifrm_top + viewport_height;
	} else {

		ifrm_bottom = viewport_height - ifrm_top;
		ifrm_top = 0;
		if(ifrm_bottom < 0) { //ifrm is below viewport
			ifrm_top = -1;
			ifrm_bottom = -1;
		}
	}

	var ifrm_width = KAMENT.main_iframe.getBoundingClientRect()['width'];

	if(KAMENT.main_iframe && KAMENT.main_iframe.contentWindow) {
		if(window['pm']) {
			pm({
				target: window.frames[KAMENT.main_iframe.id],
				type:   "updateViewport", 
				data:   {'top': ifrm_top, 'bottom': ifrm_bottom, 'width': ifrm_width},
				url: KAMENT.main_iframe.contentWindow.location
			});
		}
	};

	if(ifrm_top >= 0 && ifrm_bottom >= 0)
		KAMENT.alive = 1;
}

/*KAMENT.scrollCallback = function() {
}*/

KAMENT.attachScript = function(url) {
	var head = document.getElementsByTagName("head")[0];
	var newScript = document.createElement('script');
	newScript.type = 'text/javascript';
	newScript.setAttribute('encoding', 'UTF-8');
	newScript.src = url;
	head.appendChild(newScript);
};

KAMENT.buildIframeUrl = function(current_url, page_title, params) {
	var page_name = "";
	if(window.kament_page_name)
		page_name = window.kament_page_name;

	//var url = 'http://' + kament_subdomain + '.' + this._domain + '/commentswidget.php/commentswidget/?' + 
	var url = 'http://' + kament_subdomain + '.' + this._domain + '/commentswidget/?' + 
		'page='		+ encodeURIComponent(page_name) + '&' + 
		'title='	+ encodeURIComponent(page_title) + '&' + 
		'url='		+ encodeURIComponent(current_url);

	for (p_name in params) {
		url += '&' + p_name + '=' + encodeURIComponent(params[p_name]);
	}

	return url;
}

KAMENT.getCurrentTitle = function() {
	var t = this.getMetaValue('og:title');
	if(!t)
		t = document.title;

	if(t.length > 255)
		t = t.substring(0,255);

	return t;
}

KAMENT.getCurrentURL = function() {

	var search_part = window.location.search;
	if(search_part) {
		if(search_part.charAt(0) == '?')
			search_part = search_part.substring(1); 

		var search_arr = search_part.split('&');
		search_arr.sort();

		search_part = '?' + search_arr.join('&');
	}
	
	var url = window.location.protocol + '//' + window.location.hostname + window.location.pathname;
	if(window.location.port)
		url += ':' + window.location.port;

	url += search_part; 

	return url;
}

KAMENT.closeModal = function()
{
	if(!KAMENT.modal_opened)
		return;

	var popup_wnd = document.getElementById(this._popupWnd);
	var modal_backdrop = document.getElementById(this._popupWnd + "_backdrop");

	KAMENT.removeClass(modal_backdrop, "KAMENT-in");
	KAMENT.removeClass(popup_wnd, "KAMENT-in");

	setTimeout(function(){popup_wnd.style.display = "none"; modal_backdrop.style.display="none";}, 300);

	if(KAMENT.modal_onclose) {
		KAMENT.modal_onclose();
		KAMENT.modal_onclose = null;
	}
}

KAMENT.openModal = function(html, onclose)
{
	KAMENT.modal_opened = true;
	var popup_wnd = document.getElementById(this._popupWnd);
	var modal_backdrop = document.getElementById(this._popupWnd + "_backdrop");

	if(!popup_wnd) {
		popup_wnd = document.createElement("div"); 
		popup_wnd.setAttribute("id", this._popupWnd); 
		popup_wnd.setAttribute("tabindex", "-1"); //makes div focusable
		popup_wnd.setAttribute("style", "display:none;"); 
		popup_wnd.setAttribute("class", "kament-modal kament-fade"); 
		document.body.appendChild(popup_wnd);
	}

	if(!modal_backdrop) {
		modal_backdrop = document.createElement("div"); 
		modal_backdrop.setAttribute("id", this._popupWnd + "_backdrop"); 
		modal_backdrop.setAttribute("style", "display:none;"); 
		modal_backdrop.setAttribute("class", "kament-modal-backdrop kament-fade"); 
		document.body.appendChild(modal_backdrop);
	}

	popup_wnd.innerHTML = html;
	KAMENT.modal_onclose = onclose;

	popup_wnd.setAttribute("style", "display:block;");
	modal_backdrop.setAttribute("style", "display:block;");

	setTimeout(function(){
			KAMENT.addClass(modal_backdrop, "kament-in");
			KAMENT.addClass(popup_wnd, "kament-in");
		}, 10);
	popup_wnd.focus();
}

KAMENT.openProfileSettings = function()
{
	if(this.modal_id == undefined)
		this.modal_id = 0; //ffox trick, need new iframe id each time to use onload
	else
		this.modal_id++ 

	var iframe_src = 'http://' + kament_subdomain + '.' + KAMENT._domain + '/profileSettingsWidget';
	var iframe_id = 'kament_settings_' + this.modal_id;
	var html = '<iframe ' +
			' src="' + iframe_src + '"' +
			' id="' + iframe_id + '"' +
			' name="kament_settings_' + this.modal_id + '"' +
			' style="border:none;width:650px"' +
			' scrolling="no"' +
			' frameBorder="0"' +
			' onload="KAMENT.FrameManager.registerFrame(document.getElementById(\'' + iframe_id + '\'))"' +
		'></iframe>';

	KAMENT.openModal(html, function(){
		//TODO: this is shit.
		//Update this to trigger only when user *saves* and his settings are really updated
		pm({
			target: window.frames[KAMENT.main_iframe.id]
			,type:   "userSettingsUpdated"
		});
	});
};

KAMENT.loginUpdate = function(data) {
	pm({
		target: window.frames[KAMENT.main_iframe.id]
		,type:   "loginUpdate"
	});
}

KAMENT.onKeyDown = function(evt) {
	evt = evt || window.event;
	var charCode = evt.keyCode || evt.which;
	if (charCode == 27) {
		KAMENT.closeModal();
	}
	KAMENT.alive = 1;
}

KAMENT.openAttachView = function(attachments, current_id)
{
	//TODO: gallery of all attached images, loop through them with 'onclick';
	html = '<div class="kament-attach-view"><img src="' + attachments[current_id]['url'] + '"/></div>';
	KAMENT.openModal(html);

	KAMENT.addEvent(document, 'click', KAMENT.closeModalOnClick);
};

KAMENT.closeModalOnClick = function(evt) {
	console.log('CLICK!');
	KAMENT.closeModal();
	document.removeEventListener('click', KAMENT.closeModalOnClick);
};

KAMENT.scrollTo = function(scroll_y)
{
	if(!KAMENT.main_iframe)
		return;

	if(scroll_y < 0)
		scroll_y = 0;

	var ifrm_top = KAMENT.getOffset( KAMENT.main_iframe ).top; 
	scroll_y += ifrm_top;

	var docTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;

	var tick = 20;
	var iterations = 500/tick;
	var scrollPerTick = (scroll_y-docTop)/iterations;

	var scrollInt = setInterval(function(){
		window.scrollBy(0, scrollPerTick);

		iterations--;
		if(iterations <= 0)
			clearInterval(scrollInt);

	}, tick);
}

KAMENT.registerCallback = function (ev_name, cbfunc)
{
	pm.bind('Event-' + ev_name, cbfunc);        
}

KAMENT.FrameManager = {
	init : function() {
		pm.bind("openProfileSettings", function(data) {
			KAMENT.openProfileSettings();
		});

		pm.bind("openAttachView", function(data) {
			KAMENT.openAttachView(data['attachments'], data['current_id']);
		});

		pm.bind("closeModal", function(data) {
			KAMENT.closeModal();
		});

		pm.bind("KAMENT.scrollTo", function(scroll_y) {
			KAMENT.scrollTo(scroll_y);
		});

		//vestiru tmp
		//pm.bind("openGMAboutPage", function(scroll_y) {
			////KAMENT.scrollTo(scroll_y);
		//});
		pm.bind("openPage", function(data) {
		//	alert('openPage: ' + data);
			window.location.href = data;
		});


	},

	registerFrame : function(frame) {
		var data = {
			id: frame.id
		};

		if(window.SVGameMasterPayload) {
			data['gamemasterPayload'] = JSON.stringify( window.SVGameMasterPayload );
		}

		pm({
			target: window.frames[frame.id],
			type:   "register", 
			data:   data,
			url: frame.contentWindow.location
		});

		pm.bind(frame.id, function(data) {
			var iframe = document.getElementById(data.id);
			if (iframe == null) return;
			iframe.style.height = (data.height).toString() + "px";            
		});        


	}

};
KAMENT.Init();
