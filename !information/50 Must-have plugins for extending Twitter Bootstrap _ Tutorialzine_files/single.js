$(function(){

	// Syntax highlighting

	if(window.hljs){

		$('#post pre:not(.inline-editor pre):not(.challenge pre)').each(function(i, e) {

			var elem = $(this);
			var type = elem.attr('class') || elem.data('type');
			type = type.replace('brush:','').replace('js','javascript').replace('plain','no-highlight');

			elem.wrapInner('<code class="'+type+'" />');

			// Show the line numbers

			var lines = elem.text().match(/\n/g);
			lines = (lines ? lines.length : 0) + 1;

			if(lines > 1){

				var l = '';
				for(var i=0;i<lines;i++){
					l+=(i+1)+'\n';
				}

				elem.attr('data-lines',l);
			}
		});

		hljs.tabReplace = '    ';
		hljs.initHighlightingOnLoad();
	}

	// Challenge post

	var isChallenge = false,
		includeSnippets = {},
		defaultSnippets = {};

	var challenge = $('#post .challenge');
	
	if(challenge.length){

		// This post is a challenge
		isChallenge = true;

		// Scan the include 

		challenge.find('pre[data-id]').each(function(){
			var pre = $(this);
			includeSnippets[pre.data('id')] = pre.text();
			pre.remove();
		});

		// Find the default code

		challenge.find('pre[data-default]').each(function(){
			var pre = $(this);
			
			if(!pre.data('type')) return true;

			defaultSnippets[pre.data('type')] = pre.text();
			pre.remove();
		});

		// remove the challenge pre elements that might be left on the page
		challenge.find('pre').remove();
		
		
		// Make the buttons point to the correct sharing urls
		challenge.find('.sharing-button a').each(function(){
			
			this.href = this.href.replace('{URL}', encodeURIComponent(the_permalink));
			
		}).click(function(e){
			
			e.preventDefault();

			var link = $(this),
				url = this.href.replace('{TEXT}', encodeURIComponent(challenge.find('.score').text() + '!! // ' + the_title)),
				w = link.data('width') || 500,
				h = link.data('height') || 400,
				left = (screen.width/2)-(w/2),
				top = (screen.height/2)-(h/2);
			
			window.open(url, 'Tutorialzine Challenge', 'toolbar=no, location=no, directories=no, status=no,'+
					' menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		});
		
		
		// A helper function for controlling the percentage
		
		var totalEditors = $('#post .inline-editor').length,
			done = [];
		
		challenge.success = function(id){
			if(done.indexOf(id) === -1){
				done.push(id);
			}
			
			challenge.find('.score').text( Math.round(100*(done.length/totalEditors)) + '%');
		};
				
		var postBody = $('#post .post-body');
		
		$(window).on('resize', function(){
			
			// On resize, scale the challenge toolbar to match the width 
			// of the article body and its position
			
			challenge.width(postBody.outerWidth());
			challenge.css({left:Math.round(postBody.offset().left)});
			
		}).resize();
	}


	// Inline code editor
	
	$('#post .inline-editor').each(function(index){
		
		var editor = $(this);
		var toolbar = $('<div class="toolbar"></div>').appendTo(editor);

		// Create the ace editor
		var elem = $('<div>').appendTo(editor);
		var aceEditor = ace.edit(elem[0]);
		aceEditor.setTheme('ace/theme/tomorrow');
		
		// Result holder
		var result = $('<div class="result">').appendTo(editor);
		result.append('<div class="inline-log"></div>');
		result.append('<a class="button blue">Edit</a>');

		result.find('.button').click(function(){
			result.animate({top:'100%'},'fast', function(){
				result.find('iframe').remove();
				result.find('.inline-log').empty();
			});
		});


		// Scan for included system code pre

		var systemCode = [];

		editor.find('pre[data-system]').each(function(){

			var pre = $(this);
			systemCode.push({
				code: pre.text(),
				include: pre.data('include')
			});

			pre.remove();
		});

		// Flag that blocks multiple _SUCCESS(), _FAIL() calls.
		var validated = false;

		window['inlineEditorMessage'+index] = function(arr, type){
				
			if(type == 'error'){
				
				if(validated){
					// A _SUCCESS() / _FAIL() call has already been 
					// made or there has been an error. Ignore the subsequent ones
					return;
				}
				
				// Prevent further success/error messages.
				validated = true;
				
				result.find('.inline-log').append('<div class="error">' + arr[0] + ' on line ' + arr[2] + '</div>');
			}
			else if(type == 'success' || type == 'fail'){
				
				if(validated) return;
				validated = true;
				
				if(type == 'success' && isChallenge){
					challenge.success(index);
				}
				
				if(!arr){
					arr = (type == 'success' ? 'Success!' : 'Fail!');
				}
				
				var msg = $('<div class="' + type + '"><span>' + arr + ' <i>click to edit</i></span></div>').hide();
				
				msg.click(function(){
					msg.fadeOut(function(){
						msg.remove();
					});
					
					result.find('.button').click();
				});
				
				msg.appendTo(result).delay(1000).fadeIn();
			}
			else{
				
				// Log message
				
				var arr = Array.prototype.slice.call(arr);

				try {

					$.each(arr, function(k) {
						if ($.isPlainObject(this) || $.isArray(this)) {
							arr[k] = JSON.stringify(this);
						}
					});

				} catch (e) {}

				result.find('.inline-log').append('<div class="log">' + arr.join(' ').replace(/</g, '&lt;').replace(/>/g, '&gt;') + '</div>');
				window.console && console.log.apply(console, arr);
			}
		};


		// Monitor clicks on the span tabs
		toolbar.on('click', 'span', function(e, simulated){
			
			toolbar.find('span').removeClass('active');
			
			var tab = $(this);

			aceEditor.setSession(tab.data('session'));
			aceEditor.resize();
			
			tab.addClass('active').data('editor');
			
			if(!simulated){
				aceEditor.focus();
			}

			// Make the editor read only, if this is a protected tab
			aceEditor.setReadOnly(tab.hasClass('locked'));
		});
		
		rebuildEditor();
		
		// Add the reset button
		$('<div class="reset" title="Reset"></div>').click(function(){
			
			if(confirm('Reset the editor? All your changes will be lost.')){
				rebuildEditor(true);
			}
		}).appendTo(toolbar);
		
		// The Run button
		var run = $('<a class="button blue">Run</a>').click(function(){
			
			var code = {};
			
			// Permit validation messages to be sent
			validated = false;
			
			// Remove old messages that might be left
			result.find('.success, .fail').remove();

			toolbar.find('span').each(function(tabIndex){
				var tab = $(this);

				code[tab.data('type')] = code[tab.data('type')] || [];

				// Save the tab contents to sessionStorage if it is not protected
				if(!tab.data('protected')){
					sessionStorage[the_permalink + '#' + index + '-' + tabIndex] = tab.data('session').getValue();
				}

				if(tab.data('type') == 'js' && tab.data('protected')){

					// When protected tabs include code, it is added to their closure, so it is
					// unaccesssible from the user code

					var include = '';

					if(tab.data('include') && includeSnippets[tab.data('include')] !== undefined){
						include = includeSnippets[tab.data('include')] + ';';
					}

					// If this is a protected JS tab, place the code at the beginning in a closure.
					code['js'].unshift('(function(){ ' + include + tab.data('session').getValue() + ' })();');

				}
				else if(tab.data('type') == 'js' && tab.data('include')){

					// When user js tabs include code, it is added as a regular 
					// script to the page, before the pre code

					code['js'].push(includeSnippets[tab.data('include')]);

					code['js'].push(tab.data('session').getValue());

				}
				else{
					code[tab.data('type')].push(tab.data('session').getValue());
				}

			});
			
						
			if(systemCode.length){
				
				// Place all the system code before the user supplied code
				
				$.each(systemCode, function(){
					
					var c = '';
					
					if(this.include){
						c = '(function(){' + includeSnippets[this.include] + '; ' + this.code + '})();'
					}
					else{
						c = this.code;
					}

					code['js'].unshift(c);
				});
				
			}
			
			var html = buildFrameMarkup(code, editor.data('includes'));
			
			// Create the iframe
			var iframe = document.createElement('iframe');
			
			iframe.src = 'about:blank';
			iframe.frameBorder="0";
			iframe.height = result.outerHeight() - 85;
			
			result.remove('iframe').append(iframe);
			
			iframe.contentWindow.document.open('text/html', 'replace');
			iframe.contentWindow.document.write(html);
			iframe.contentWindow.document.close();

			result.animate({top:0}, 'fast');
			
		}).appendTo(editor);
		
		if(editor.data('autorun')){
			setTimeout(function(){
				run.click();
			}, 250 + (250 * index));
		}
		
		function rebuildEditor(force) {

			// Todo: restore the content from localStorage if it exists.
			// Create a parameter to this function, which destroys the cached
			// content in localStorage.

			toolbar.find('span').remove();

			// Regular code tabs
			
			editor.find('pre').each(function(preIndex) {

				var pre = $(this),
					type = $.trim(pre.data('type')),
					text = sessionStorage[the_permalink + '#' + index + '-' + preIndex],
					language = type.replace('js', 'javascript');

				if(force || !text){
					text = pre.text();
				}

				// Create the tabs
				var tab = $('<span>' + (pre.data('name') || type) + '</span>');

				tab.data('type', type);
				tab.data('session', createEditSession(text, language));

				// Does this pre element include code declared
				// as default in the challenge section?

				if(pre.data('include')){
					tab.data('include', pre.data('include'));
				}

				// If this is a "protected" editor, disable editing.
				if(pre.data('protected')){
					tab.addClass('locked');
					tab.data('protected', true);
				}

				tab.appendTo(toolbar);
			});
			
			// Simulate a click on the first tab
			toolbar.find('span').first().trigger('click', [true]);
		}
		
		function createEditSession(text, mode){
			// Initialize a new edit session
			var EditSession = ace.require('ace/edit_session').EditSession;
			var UndoManager = ace.require('ace/undomanager').UndoManager;

			var session = new EditSession(text, 'ace/mode/' + mode);
			session.setUndoManager(new UndoManager());
			
			return session;
		}
		
		function buildFrameMarkup(code, libraryURLs){

			// Use the default code that has been added to the challenge element,
			// if one exists, and if there are no such code types in the editor.

			for(var k in defaultSnippets){
				if(!code.hasOwnProperty(k)){
					code[k] = [defaultSnippets[k]];
				}
			}

			var headjs = '<script>{js}</script>';
			
			if($.type(libraryURLs) === 'array' && libraryURLs.length > 0){
				headjs = '<script src="http://cdnjs.cloudflare.com/ajax/libs/headjs/0.99/head.min.js"></script>';
				headjs += '<script>head.js("' + libraryURLs.join('", "') +'", function(){ {js} });</script>';
			}
			else {

				headjs = '<script>document.addEventListener("DOMContentLoaded", function _l(){\n\
							document.removeEventListener("DOMContentLoaded", _l, false);\n\
							{js}\n\
							(function(){var _d = document.createEvent("Event");\n\
								_d.initEvent("DOMContentLoaded", true, true);\n\
								window.document.dispatchEvent(_d);\n\
							})();\n\
						}, false);</script>';
			}
			
			if(code.js){
				// add line checking code
				var tmp = "window.onerror = function(){\
					window.parent.inlineEditorMessage"+index+"(arguments,'error'); return true;};\
					window.console = window.console || {}; console.log = function(){\
						window.parent.inlineEditorMessage"+index+"(arguments,'log');};";
				
				if(isChallenge){
					tmp += 'window._SUCCESS = function(msg){ window.parent.inlineEditorMessage'+index+'(msg,"success");};';
					tmp += 'window._FAIL = function(msg){ window.parent.inlineEditorMessage'+index+'(msg,"fail");};';
				}
				
				for(var i = 0; i < code.js.length; i++){

					// Create a new script element for every js pre

					tmp += "var script = document.createElement('script');\
						script.textContent = " + JSON.stringify(code.js[i]).replace(/<\/script>/g, '<\\/script>') + ";\
						document.body.appendChild(script);";		
				}
			
				
				code.js = tmp;
			}
			
			if(code.html) code.html = code.html.join('');
			if(code.css) code.css = code.css.join('');

			var html = "<!doctype html>\n\
						<html>\n\
							<head>\n\
								<meta charset='utf-8'>\n\
								<style>{css}</style>\n\
								" + headjs + "\n\
							</head>\n\
							<body>\n\
							{html}\n\
							</body>\n\
						</html>";

			return html.replace(/\{(\w+)\}/g, function(match, group){
				if(group in code) {
					
					if(group == 'html' || group == 'js'){
						// HTML and JS don't need to be escaped
						return code[group];
					}
					
					return code[group].replace(/</g, '&lt;').replace(/>/g, '&gt;');
				}
				return '';
			});
		}
	});


	// Quizzes

	$('.quiz').each(function(){
			
		var quiz = $(this),
			panels = quiz.find('.panel'),
			button = quiz.find('.button'),
			progress = quiz.find('.progress');

		var rightAnswers = 0,
			wrongAnswers = 0;

		var totalQuestions = panels.filter('[data-type=question]').length,
			currentPanel = 0, currentQuestion = 0;

		var activePanel = panels.first();

		quiz.show();
		updateProgress();
		
		// Show the first panel of the quiz
		activePanel.show();

		properPanelTitle();

		// Listen for clicks on the answer button

		button.click(function(){

			if(button.text() == 'Answer'){
				evaluateAnswer();
			}
			else {
				nextPanel();
			}

		});

		function evaluateAnswer(){
			
			if(isAnswerCorrect()) {
				rightAnswers++;
				
				// Mark the button as success
				button.addClass('success');
			}
			else {
				wrongAnswers++;
				
				// Mark the button as a failure
				button.addClass('fail');
				
				// Mark the selected LI as wrong
				activePanel.find('ul input:checked').closest('li').addClass('wrong');
			}

			// Mark the valid answer as the right one
			activePanel.find('ul li[data-valid=1]').addClass('right');

			makeButtonNext();
			
		}

		function makeButtonNext(){
			button.text('Next');
		}

		function makeButtonAnswer(){
			button.removeClass('success fail').text('Answer');
		}
		
		function makeButtonSkip(){
			button.removeClass('success fail').text('Skip');
		}

		function isAnswerCorrect(){
			return activePanel.find('ul li[data-valid=1] input:checked').length == 1;
		}

		function updateProgress(){
			progress.html('<b>' + (currentQuestion + 1) +  '</b> of <b>'+ totalQuestions +'</b>');
		}

		function nextPanel(){
			
			activePanel.hide();
			
			activePanel = panels.eq(++currentPanel);
			activePanel.show();
			
			properPanelTitle();
			
			if(activePanel.is('[data-type=question]')) {
				currentQuestion++;
				makeButtonAnswer();
				updateProgress();
				progress.show();
			}
			else if (activePanel.is('[data-type=ad]')) {
				makeButtonSkip();
				progress.hide();
			}
			else {
				finish();
			}
		}
		
		function properPanelTitle(){
			if(activePanel.find('h3').length){
				// Show the panel's h3
				quiz.find('> h3').hide();
			}
			else {
				// otherwise show the quiz's h3
				quiz.find('> h3').show();
			}
		}
		
		function finish(){
			
			// Clean up the quiz
			quiz.find('.panel:not([data-type=finish]), .bottom').remove();
			
			var fin = quiz.find('.panel[data-type=finish]');
			
			fin.find('.result').html('You guessed <b>' + rightAnswers + '</b> out of <b>' + totalQuestions + '</b>!');
			
			fin.find('.share-quiz a').each(function(){

				this.href = this.href.replace('{URL}', encodeURIComponent(the_permalink));

			}).click(function(e){

				e.preventDefault();

				var url = this.href.replace('{TEXT}', encodeURIComponent( + rightAnswers + ' out of ' + totalQuestions + '!! // ' + the_title)),
					w = 500,
					h = 400,
					left = (screen.width/2)-(w/2),
					top = (screen.height/2)-(h/2);

				window.open(url, 'Tutorialzine Challenge', 'toolbar=no, location=no, directories=no, status=no,'+
						' menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
			});
			
		}

	});


	// Sidebar post tabs

	$('#sidebar .sidebar-group.posts h5').click(function(){
		var elem = $(this);

		elem.addClass('active').siblings().removeClass('active');
		elem.parent().find('#'+elem.data('tab')).addClass('active');
	});


	// Follow Button Sidebar

	setTimeout(function(){
		var iframe = '<iframe scrolling="no" frameborder="0" style="width:300px; height:20px"' +
						'src="http://platform.twitter.com/widgets/follow_button.html?screen_name=tutorialzine'+
						'&amp;link_color=008de6&amp;text_color=aaaaaa" allowtransparency="true"></iframe>';

		$('#followTwitterButton').html(iframe);
	},3000);

	// Newsletter subscription form

	$('#newsletterSubscribe form').submit(function(e){
		e.preventDefault();

		var hider = $('#newsletterSubscribe .hider').empty();
		var errHolder = $('#newsletterSubscribe .errHolder').empty();
		var newsletterForm = $(this);

		hider.stop().fadeTo('fast',0.75);

		newsletterHelper($('#newsletterSubscribe input[type=text]').val(), function(status, code){
			switch(status){
				case 'success':
					hider.stop().fadeTo('fast',1).html('Thank you! Check your email for instructions.');
					newsletterForm.remove();
					
					var from = '';
					
					switch(newsletterForm.data('origin')){
						case 'front-page':
							from = 'From Front Page';
							break;
						case 'article':
							from = 'From Article';
							break;
					}
					
					trackEvent('Signup', 'Newsletter', from);
					
					break;
					
				case 'registered':
					hider.stop().fadeTo('fast',0,function(){$(hider.hide())});
					errHolder.html('You\'ve already signed up.');
					break;
					
				case 'invalid':
					hider.stop().fadeTo('fast',0,function(){$(hider.hide())});
					errHolder.html('Invalid email.');
					break;
			}
		});

	});
	
	function newsletterHelper(email, cb){
		
		$.post('/webdev-newsletter/',{ email: email , submit:'1' }, function(r,xhr){

			// Match the responses

			if(r.match('JS:SUCCESS')){
				cb('success')
			}
			else if(r.match('JS:REGISTERED')){
				cb('registered', r.match(/JS:REGISTERED:(\w+)/)[1]);
			}
			else {
				cb('invalid');
			}

		}, 'html');
	}

	// Member area
	
	var signin = $('.mContainer .hero1 .signin');
	
	if(signin.length){

		$('body').click(function(){
			signin.removeClass('expand');
		});
		
		signin.click(false);
		
		signin.find('.login').click(function(e){
			e.preventDefault();

			signin.addClass('expand');
			signin.find('input').focus();
		});
		
		signin.find('.go').click(function(e){
			e.preventDefault();
			
			$.post('/webdev-newsletter/', {check: signin.find('input').val()}, function(r){
				if(/\w+/.test(r)){
					window.location = '/webdev-newsletter/'+r+'/';
				}
				else{
					// show the error message that the person is not logged in
					showFlashMessage('This email is not registered. Please sign up first!', 'red');
				}
			}, 'html');
		});
		
		$('.mMessageSuccess').each(function(){
			var el = $(this).remove();
			showFlashMessage(el.text(), 'green');
			trackEvent('Signup', 'Newsletter', 'From Member Page');
		});
		
		$('.mMessageError').each(function(){
			var el = $(this).remove();
			showFlashMessage(el.text(), 'red');
		});
	}

	function showFlashMessage(message, cls){
		$('.flashMessage').remove();
		var msg = $('<div class="flashMessage '+cls+'">');
		
		msg.text(message)
		   .hide()
		   .appendTo('body')
		   .fadeIn('fast')
		   .delay(5000)
		   .fadeOut('fast')
		   .click(function(){
				msg.queue('fx',[]);
				msg.fadeOut('fast');
		   });
	}

	// Sidebar Newsletter Form
	
	var newsletterSidebar = $('#newsletter-sidebar');
	
	newsletterSidebar.each(function(){
		
		if(/^http:\/\/tutorialzine.com/.test(document.referrer)){
			// This visit came from tzine. Do nothing
			return;
		}
		
		// Remove the other newsletter subscription
		$('#post #newsletterSubscribe').remove();
		
		newsletterSidebar.find('a').click(function(){
			trackEvent('Preview', 'Newsletter', 'From Sidebar');
		});
		
		(function(){
			
			var win = $(window),
				doc = $(document),
				visible = false,
				sidebar = $('#sidebar');

			win.on('scroll resize', function () {
				
				if (win.scrollTop() >= sidebar.height() + 300) {
					if(!visible){
						visible = true;
						newsletterSidebar.stop(true,true).fadeIn('fast');
					}
				}
				else{
					if(visible){
						visible = false;
						newsletterSidebar.stop(true,true).fadeOut('fast', function(){
							// If the banner is hidden by the media query, fadeOut will not animate
							// it and it won't set display:none. Fixing this:
							newsletterSidebar.hide();
						});						
					}
						
					
				}
			}).resize();
			
			var hider = newsletterSidebar.find('.hider'),
				form = newsletterSidebar.find('form'),
				working = false;
			
			form.submit(function(e){
				e.preventDefault();
				
				if(working) return;
				working = true;
				
				hider.fadeTo('fast', 0.4);
				
				newsletterHelper(newsletterSidebar.find('input[type=text]').val(), function(status){

					working = false;
					
					switch(status){
						case 'success':
							
							form.remove();
							
							newsletterSidebar.fadeOut('fast', function(){
								newsletterSidebar.remove();
							});
							
							showFlashMessage('Thank you! Check your email for instructions.', 'green');
							
							trackEvent('Signup', 'Newsletter', 'From Article Sidebar');
							break;

						case 'registered':
							hider.stop().fadeTo('fast',0,function(){$(hider.hide())});
							showFlashMessage('You are already registered!', 'red');
							break;

						case 'invalid':
							hider.stop().fadeTo('fast',0,function(){$(hider.hide())});
							showFlashMessage('Invalid email.', 'red');
							break;
					}
				});
			});
		})();
	});
	
	

	// The search text field

	var searchIcon = $('.searchIcon');

	searchIcon.click(function(e){
		e.preventDefault();

		if(searchIcon.is('.active') && $(e.target).is(searchIcon)){
			searchIcon.removeClass('active');
		}
		else{
			searchIcon.addClass('active');
			searchIcon.find('input').focus();
		}

	});

	$('body').click(function(e){

		if(	searchIcon.is('.active') &&
			!$(e.target).is('.searchIcon, .searchIcon form, .searchIcon input')){

			searchIcon.removeClass('active');
		}

	});


	var shareBar = $('#shareBar');

	if(shareBar.length && window.the_title && window.the_permalink){

		var html = '<iframe allowtransparency="true" frameborder="0" scrolling="no"' +
			'src="http://platform.twitter.com/widgets/tweet_button.html?url=' + encodeURIComponent(the_permalink) + '&amp;text=' + encodeURIComponent(the_title) + '&amp;via=tutorialzine&amp;count=horizontal"'+
			'style="width:105px; height:22px;">' +
		'</iframe>' +

		'<iframe src="http://www.facebook.com/plugins/like.php?href=' + encodeURIComponent(the_permalink) + '&amp;layout=button_count&amp;show_faces=false&amp;width=350&amp;action=like&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>' +

		'<script src="https://apis.google.com/js/plusone.js" async></script> <g:plusone href="' + encodeURIComponent(the_permalink) + '" size="medium"></g:plusone>';

		setTimeout(function(){
			shareBar.html(html).fadeIn();
		}, 2000);

	}
	
	// Turn the navigation menu into an ul
	
	var navItems = [{text:'Home', url:'/' }],
		navSelect, nav = $('nav'), win = $(window),
		index = nav.find('a').filter('.active').index();
	
	index = (index !== -1) ? index + 1 : 0;
	
	win.resize(function(){
		
		if(win.width() < 600){
			
			if(!navSelect){

				navSelect = $('<select>');

				Array.prototype.push.apply(navItems, nav.find('a').map(function(){
					return {text: this.textContent, url: this.href};
				}).get());

				$.each(navItems, function(){
					navSelect.append($('<option>',{text:this.text}));
				});

				navSelect.on('change', function(){
					window.location = navItems[this.selectedIndex].url;
				});
				
				navSelect.prop('selectedIndex', index);
				
				navSelect.appendTo('header');
			}
			
			nav.hide();
			navSelect.show();
			
		}
		else{
			nav.show();
			navSelect && navSelect.hide();
		}
		
		
	}).resize();


	// The book banner
	
	var jqts_banner = $('#jq-trickshots-fixed'),
		ts = $.now()/1000;
	
	var hide_banner = (	window.localStorage && 
						localStorage.hideBanner &&
						localStorage.hideBanner > ts);
	
	if(jqts_banner.length && !hide_banner){
		
		jqts_banner.delay(2000).fadeIn('slow').find('a.close').click(function(){
			
			// Hide the banner for a week
			localStorage.hideBanner = ts + 7*24*60*60;
			jqts_banner.fadeOut();
		});

	}
	
	// Event tracking
		
	function trackEvent(){
		var arr = ['_trackEvent'];
		arr.push.apply(arr,arguments);
		
		_gaq && _gaq.push(arr);
	}
		
});

addComment={moveForm:function(d,f,i,c){var m=this,a,h=m.I(d),b=m.I(i),l=m.I("cancel-comment-reply-link"),j=m.I("comment_parent"),k=m.I("comment_post_ID");if(!h||!b||!l||!j){return}m.respondId=i;c=c||false;if(!m.I("wp-temp-form-div")){a=document.createElement("div");a.id="wp-temp-form-div";a.style.display="none";b.parentNode.insertBefore(a,b)}h.parentNode.insertBefore(b,h.nextSibling);if(k&&c){k.value=c}j.value=f;l.style.display="";l.onclick=function(){var n=addComment,e=n.I("wp-temp-form-div"),o=n.I(n.respondId);if(!e||!o){return}n.I("comment_parent").value="0";e.parentNode.insertBefore(o,e);e.parentNode.removeChild(e);this.style.display="none";this.onclick=null;return false};try{m.I("comment").focus()}catch(g){}return false},I:function(a){return document.getElementById(a)}};
;var hljs=new function(){function l(o){return o.replace(/&/gm,"&amp;").replace(/</gm,"&lt;").replace(/>/gm,"&gt;")}function b(p){for(var o=p.firstChild;o;o=o.nextSibling){if(o.nodeName=="CODE"){return o}if(!(o.nodeType==3&&o.nodeValue.match(/\s+/))){break}}}function h(p,o){return Array.prototype.map.call(p.childNodes,function(q){if(q.nodeType==3){return o?q.nodeValue.replace(/\n/g,""):q.nodeValue}if(q.nodeName=="BR"){return"\n"}return h(q,o)}).join("")}function a(q){var p=(q.className+" "+q.parentNode.className).split(/\s+/);p=p.map(function(r){return r.replace(/^language-/,"")});for(var o=0;o<p.length;o++){if(e[p[o]]||p[o]=="no-highlight"){return p[o]}}}function c(q){var o=[];(function p(r,s){for(var t=r.firstChild;t;t=t.nextSibling){if(t.nodeType==3){s+=t.nodeValue.length}else{if(t.nodeName=="BR"){s+=1}else{if(t.nodeType==1){o.push({event:"start",offset:s,node:t});s=p(t,s);o.push({event:"stop",offset:s,node:t})}}}}return s})(q,0);return o}function j(x,v,w){var p=0;var y="";var r=[];function t(){if(x.length&&v.length){if(x[0].offset!=v[0].offset){return(x[0].offset<v[0].offset)?x:v}else{return v[0].event=="start"?x:v}}else{return x.length?x:v}}function s(A){function z(B){return" "+B.nodeName+'="'+l(B.value)+'"'}return"<"+A.nodeName+Array.prototype.map.call(A.attributes,z).join("")+">"}while(x.length||v.length){var u=t().splice(0,1)[0];y+=l(w.substr(p,u.offset-p));p=u.offset;if(u.event=="start"){y+=s(u.node);r.push(u.node)}else{if(u.event=="stop"){var o,q=r.length;do{q--;o=r[q];y+=("</"+o.nodeName.toLowerCase()+">")}while(o!=u.node);r.splice(q,1);while(q<r.length){y+=s(r[q]);q++}}}}return y+l(w.substr(p))}function f(q){function o(s,r){return RegExp(s,"m"+(q.cI?"i":"")+(r?"g":""))}function p(y,w){if(y.compiled){return}y.compiled=true;var s=[];if(y.k){var r={};function z(A,t){t.split(" ").forEach(function(B){var C=B.split("|");r[C[0]]=[A,C[1]?Number(C[1]):1];s.push(C[0])})}y.lR=o(y.l||hljs.IR,true);if(typeof y.k=="string"){z("keyword",y.k)}else{for(var x in y.k){if(!y.k.hasOwnProperty(x)){continue}z(x,y.k[x])}}y.k=r}if(w){if(y.bWK){y.b="\\b("+s.join("|")+")\\s"}y.bR=o(y.b?y.b:"\\B|\\b");if(!y.e&&!y.eW){y.e="\\B|\\b"}if(y.e){y.eR=o(y.e)}y.tE=y.e||"";if(y.eW&&w.tE){y.tE+=(y.e?"|":"")+w.tE}}if(y.i){y.iR=o(y.i)}if(y.r===undefined){y.r=1}if(!y.c){y.c=[]}for(var v=0;v<y.c.length;v++){if(y.c[v]=="self"){y.c[v]=y}p(y.c[v],y)}if(y.starts){p(y.starts,w)}var u=[];for(var v=0;v<y.c.length;v++){u.push(y.c[v].b)}if(y.tE){u.push(y.tE)}if(y.i){u.push(y.i)}y.t=u.length?o(u.join("|"),true):{exec:function(t){return null}}}p(q)}function d(D,E){function o(r,M){for(var L=0;L<M.c.length;L++){var K=M.c[L].bR.exec(r);if(K&&K.index==0){return M.c[L]}}}function s(K,r){if(K.e&&K.eR.test(r)){return K}if(K.eW){return s(K.parent,r)}}function t(r,K){return K.i&&K.iR.test(r)}function y(L,r){var K=F.cI?r[0].toLowerCase():r[0];return L.k.hasOwnProperty(K)&&L.k[K]}function G(){var K=l(w);if(!A.k){return K}var r="";var N=0;A.lR.lastIndex=0;var L=A.lR.exec(K);while(L){r+=K.substr(N,L.index-N);var M=y(A,L);if(M){v+=M[1];r+='<span class="'+M[0]+'">'+L[0]+"</span>"}else{r+=L[0]}N=A.lR.lastIndex;L=A.lR.exec(K)}return r+K.substr(N)}function z(){if(A.sL&&!e[A.sL]){return l(w)}var r=A.sL?d(A.sL,w):g(w);if(A.r>0){v+=r.keyword_count;B+=r.r}return'<span class="'+r.language+'">'+r.value+"</span>"}function J(){return A.sL!==undefined?z():G()}function I(L,r){var K=L.cN?'<span class="'+L.cN+'">':"";if(L.rB){x+=K;w=""}else{if(L.eB){x+=l(r)+K;w=""}else{x+=K;w=r}}A=Object.create(L,{parent:{value:A}});B+=L.r}function C(K,r){w+=K;if(r===undefined){x+=J();return 0}var L=o(r,A);if(L){x+=J();I(L,r);return L.rB?0:r.length}var M=s(A,r);if(M){if(!(M.rE||M.eE)){w+=r}x+=J();do{if(A.cN){x+="</span>"}A=A.parent}while(A!=M.parent);if(M.eE){x+=l(r)}w="";if(M.starts){I(M.starts,"")}return M.rE?0:r.length}if(t(r,A)){throw"Illegal"}w+=r;return r.length||1}var F=e[D];f(F);var A=F;var w="";var B=0;var v=0;var x="";try{var u,q,p=0;while(true){A.t.lastIndex=p;u=A.t.exec(E);if(!u){break}q=C(E.substr(p,u.index-p),u[0]);p=u.index+q}C(E.substr(p));return{r:B,keyword_count:v,value:x,language:D}}catch(H){if(H=="Illegal"){return{r:0,keyword_count:0,value:l(E)}}else{throw H}}}function g(s){var o={keyword_count:0,r:0,value:l(s)};var q=o;for(var p in e){if(!e.hasOwnProperty(p)){continue}var r=d(p,s);r.language=p;if(r.keyword_count+r.r>q.keyword_count+q.r){q=r}if(r.keyword_count+r.r>o.keyword_count+o.r){q=o;o=r}}if(q.language){o.second_best=q}return o}function i(q,p,o){if(p){q=q.replace(/^((<[^>]+>|\t)+)/gm,function(r,v,u,t){return v.replace(/\t/g,p)})}if(o){q=q.replace(/\n/g,"<br>")}return q}function m(r,u,p){var v=h(r,p);var t=a(r);if(t=="no-highlight"){return}var w=t?d(t,v):g(v);t=w.language;var o=c(r);if(o.length){var q=document.createElement("pre");q.innerHTML=w.value;w.value=j(o,c(q),v)}w.value=i(w.value,u,p);var s=r.className;if(!s.match("(\\s|^)(language-)?"+t+"(\\s|$)")){s=s?(s+" "+t):t}r.innerHTML=w.value;r.className=s;r.result={language:t,kw:w.keyword_count,re:w.r};if(w.second_best){r.second_best={language:w.second_best.language,kw:w.second_best.keyword_count,re:w.second_best.r}}}function n(){if(n.called){return}n.called=true;Array.prototype.map.call(document.getElementsByTagName("pre"),b).filter(Boolean).forEach(function(o){m(o,hljs.tabReplace)})}function k(){window.addEventListener("DOMContentLoaded",n,false);window.addEventListener("load",n,false)}var e={};this.LANGUAGES=e;this.highlight=d;this.highlightAuto=g;this.fixMarkup=i;this.highlightBlock=m;this.initHighlighting=n;this.initHighlightingOnLoad=k;this.IR="[a-zA-Z][a-zA-Z0-9_]*";this.UIR="[a-zA-Z_][a-zA-Z0-9_]*";this.NR="\\b\\d+(\\.\\d+)?";this.CNR="(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)";this.BNR="\\b(0b[01]+)";this.RSR="!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|\\.|-|-=|/|/=|:|;|<|<<|<<=|<=|=|==|===|>|>=|>>|>>=|>>>|>>>=|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~";this.BE={b:"\\\\[\\s\\S]",r:0};this.ASM={cN:"string",b:"'",e:"'",i:"\\n",c:[this.BE],r:0};this.QSM={cN:"string",b:'"',e:'"',i:"\\n",c:[this.BE],r:0};this.CLCM={cN:"comment",b:"//",e:"$"};this.CBLCLM={cN:"comment",b:"/\\*",e:"\\*/"};this.HCM={cN:"comment",b:"#",e:"$"};this.NM={cN:"number",b:this.NR,r:0};this.CNM={cN:"number",b:this.CNR,r:0};this.BNM={cN:"number",b:this.BNR,r:0};this.inherit=function(q,r){var o={};for(var p in q){o[p]=q[p]}if(r){for(var p in r){o[p]=r[p]}}return o}}();hljs.LANGUAGES.bash=function(a){var g="true false";var e="if then else elif fi for break continue while in do done echo exit return set declare";var c={cN:"variable",b:"\\$[a-zA-Z0-9_#]+"};var b={cN:"variable",b:"\\${([^}]|\\\\})+}"};var h={cN:"string",b:'"',e:'"',i:"\\n",c:[a.BE,c,b],r:0};var d={cN:"string",b:"'",e:"'",c:[{b:"''"}],r:0};var f={cN:"test_condition",b:"",e:"",c:[h,d,c,b],k:{literal:g},r:0};return{k:{keyword:e,literal:g},c:[{cN:"shebang",b:"(#!\\/bin\\/bash)|(#!\\/bin\\/sh)",r:10},c,b,a.HCM,h,d,a.inherit(f,{b:"\\[ ",e:" \\]",r:0}),a.inherit(f,{b:"\\[\\[ ",e:" \\]\\]"})]}}(hljs);hljs.LANGUAGES.javascript=function(a){return{k:{keyword:"in if for while finally var new function do return void else break catch instanceof with throw case default try this switch continue typeof delete let yield const",literal:"true false null undefined NaN Infinity"},c:[a.ASM,a.QSM,a.CLCM,a.CBLCLM,a.CNM,{b:"("+a.RSR+"|\\b(case|return|throw)\\b)\\s*",k:"return throw case",c:[a.CLCM,a.CBLCLM,{cN:"regexp",b:"/",e:"/[gim]*",i:"\\n",c:[{b:"\\\\/"}]},{b:"<",e:">;",sL:"xml"}],r:0},{cN:"function",bWK:true,e:"{",k:"function",c:[{cN:"title",b:"[A-Za-z$_][0-9A-Za-z$_]*"},{cN:"params",b:"\\(",e:"\\)",c:[a.CLCM,a.CBLCLM],i:"[\"'\\(]"}],i:"\\[|%"}]}}(hljs);hljs.LANGUAGES.css=function(a){var b={cN:"function",b:a.IR+"\\(",e:"\\)",c:[a.NM,a.ASM,a.QSM]};return{cI:true,i:"[=/|']",c:[a.CBLCLM,{cN:"id",b:"\\#[A-Za-z0-9_-]+"},{cN:"class",b:"\\.[A-Za-z0-9_-]+",r:0},{cN:"attr_selector",b:"\\[",e:"\\]",i:"$"},{cN:"pseudo",b:":(:)?[a-zA-Z0-9\\_\\-\\+\\(\\)\\\"\\']+"},{cN:"at_rule",b:"@(font-face|page)",l:"[a-z-]+",k:"font-face page"},{cN:"at_rule",b:"@",e:"[{;]",eE:true,k:"import page media charset",c:[b,a.ASM,a.QSM,a.NM]},{cN:"tag",b:a.IR,r:0},{cN:"rules",b:"{",e:"}",i:"[^\\s]",r:0,c:[a.CBLCLM,{cN:"rule",b:"[^\\s]",rB:true,e:";",eW:true,c:[{cN:"attribute",b:"[A-Z\\_\\.\\-]+",e:":",eE:true,i:"[^\\s]",starts:{cN:"value",eW:true,eE:true,c:[b,a.NM,a.QSM,a.ASM,a.CBLCLM,{cN:"hexcolor",b:"\\#[0-9A-F]+"},{cN:"important",b:"!important"}]}}]}]}]}}(hljs);hljs.LANGUAGES.xml=function(a){var c="[A-Za-z0-9\\._:-]+";var b={eW:true,c:[{cN:"attribute",b:c,r:0},{b:'="',rB:true,e:'"',c:[{cN:"value",b:'"',eW:true}]},{b:"='",rB:true,e:"'",c:[{cN:"value",b:"'",eW:true}]},{b:"=",c:[{cN:"value",b:"[^\\s/>]+"}]}]};return{cI:true,c:[{cN:"pi",b:"<\\?",e:"\\?>",r:10},{cN:"doctype",b:"<!DOCTYPE",e:">",r:10,c:[{b:"\\[",e:"\\]"}]},{cN:"comment",b:"<!--",e:"-->",r:10},{cN:"cdata",b:"<\\!\\[CDATA\\[",e:"\\]\\]>",r:10},{cN:"tag",b:"<style(?=\\s|>|$)",e:">",k:{title:"style"},c:[b],starts:{e:"</style>",rE:true,sL:"css"}},{cN:"tag",b:"<script(?=\\s|>|$)",e:">",k:{title:"script"},c:[b],starts:{e:"<\/script>",rE:true,sL:"javascript"}},{b:"<%",e:"%>",sL:"vbscript"},{cN:"tag",b:"</?",e:"/?>",c:[{cN:"title",b:"[^ />]+"},b]}]}}(hljs);hljs.LANGUAGES.php=function(a){var e={cN:"variable",b:"\\$+[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*"};var b=[a.inherit(a.ASM,{i:null}),a.inherit(a.QSM,{i:null}),{cN:"string",b:'b"',e:'"',c:[a.BE]},{cN:"string",b:"b'",e:"'",c:[a.BE]}];var c=[a.BNM,a.CNM];var d={cN:"title",b:a.UIR};return{cI:true,k:"and include_once list abstract global private echo interface as static endswitch array null if endwhile or const for endforeach self var while isset public protected exit foreach throw elseif include __FILE__ empty require_once do xor return implements parent clone use __CLASS__ __LINE__ else break print eval new catch __METHOD__ case exception php_user_filter default die require __FUNCTION__ enddeclare final try this switch continue endfor endif declare unset true false namespace trait goto instanceof insteadof __DIR__ __NAMESPACE__ __halt_compiler",c:[a.CLCM,a.HCM,{cN:"comment",b:"/\\*",e:"\\*/",c:[{cN:"phpdoc",b:"\\s@[A-Za-z]+"}]},{cN:"comment",eB:true,b:"__halt_compiler.+?;",eW:true},{cN:"string",b:"<<<['\"]?\\w+['\"]?$",e:"^\\w+;",c:[a.BE]},{cN:"preprocessor",b:"<\\?php",r:10},{cN:"preprocessor",b:"\\?>"},e,{cN:"function",bWK:true,e:"{",k:"function",i:"\\$|\\[|%",c:[d,{cN:"params",b:"\\(",e:"\\)",c:["self",e,a.CBLCLM].concat(b).concat(c)}]},{cN:"class",bWK:true,e:"{",k:"class",i:"[:\\(\\$]",c:[{bWK:true,eW:true,k:"extends",c:[d]},d]},{b:"=>"}].concat(b).concat(c)}}(hljs);hljs.LANGUAGES.python=function(a){var f={cN:"prompt",b:"^(>>>|\\.\\.\\.) "};var c=[{cN:"string",b:"(u|b)?r?'''",e:"'''",c:[f],r:10},{cN:"string",b:'(u|b)?r?"""',e:'"""',c:[f],r:10},{cN:"string",b:"(u|r|ur)'",e:"'",c:[a.BE],r:10},{cN:"string",b:'(u|r|ur)"',e:'"',c:[a.BE],r:10},{cN:"string",b:"(b|br)'",e:"'",c:[a.BE]},{cN:"string",b:'(b|br)"',e:'"',c:[a.BE]}].concat([a.ASM,a.QSM]);var e={cN:"title",b:a.UIR};var d={cN:"params",b:"\\(",e:"\\)",c:["self",a.CNM,f].concat(c)};var b={bWK:true,e:":",i:"[${=;\\n]",c:[e,d],r:10};return{k:{keyword:"and elif is global as in if from raise for except finally print import pass return exec else break not with class assert yield try while continue del or def lambda nonlocal|10",built_in:"None True False Ellipsis NotImplemented"},i:"(</|->|\\?)",c:c.concat([f,a.HCM,a.inherit(b,{cN:"function",k:"def"}),a.inherit(b,{cN:"class",k:"class"}),a.CNM,{cN:"decorator",b:"@",e:"$"},{b:"\\b(print|exec)\\("}])}}(hljs);hljs.LANGUAGES.sql=function(a){return{cI:true,c:[{cN:"operator",b:"(begin|start|commit|rollback|savepoint|lock|alter|create|drop|rename|call|delete|do|handler|insert|load|replace|select|truncate|update|set|show|pragma|grant)\\b(?!:)",e:";",eW:true,k:{keyword:"all partial global month current_timestamp using go revoke smallint indicator end-exec disconnect zone with character assertion to add current_user usage input local alter match collate real then rollback get read timestamp session_user not integer bit unique day minute desc insert execute like ilike|2 level decimal drop continue isolation found where constraints domain right national some module transaction relative second connect escape close system_user for deferred section cast current sqlstate allocate intersect deallocate numeric public preserve full goto initially asc no key output collation group by union session both last language constraint column of space foreign deferrable prior connection unknown action commit view or first into float year primary cascaded except restrict set references names table outer open select size are rows from prepare distinct leading create only next inner authorization schema corresponding option declare precision immediate else timezone_minute external varying translation true case exception join hour default double scroll value cursor descriptor values dec fetch procedure delete and false int is describe char as at in varchar null trailing any absolute current_time end grant privileges when cross check write current_date pad begin temporary exec time update catalog user sql date on identity timezone_hour natural whenever interval work order cascade diagnostics nchar having left call do handler load replace truncate start lock show pragma exists number",aggregate:"count sum min max avg"},c:[{cN:"string",b:"'",e:"'",c:[a.BE,{b:"''"}],r:0},{cN:"string",b:'"',e:'"',c:[a.BE,{b:'""'}],r:0},{cN:"string",b:"`",e:"`",c:[a.BE]},a.CNM]},a.CBLCLM,{cN:"comment",b:"--",e:"$"}]}}(hljs);hljs.LANGUAGES.ini=function(a){return{cI:true,i:"[^\\s]",c:[{cN:"comment",b:";",e:"$"},{cN:"title",b:"^\\[",e:"\\]"},{cN:"setting",b:"^[a-z0-9\\[\\]_-]+[ \\t]*=[ \\t]*",e:"$",c:[{cN:"value",eW:true,k:"on off true false yes no",c:[a.QSM,a.NM]}]}]}}(hljs);hljs.LANGUAGES.coffeescript=function(c){var b={keyword:"in if for while finally new do return else break catch instanceof throw try this switch continue typeof delete debugger super then unless until loop of by when and or is isnt not",literal:"true false null undefined yes no on off ",reserved:"case default function var void with const let enum export import native __hasProp __extends __slice __bind __indexOf"};var a="[A-Za-z$_][0-9A-Za-z$_]*";var e={cN:"title",b:a};var d={cN:"subst",b:"#\\{",e:"}",k:b,c:[c.BNM,c.CNM]};return{k:b,c:[c.BNM,c.CNM,c.ASM,{cN:"string",b:'"""',e:'"""',c:[c.BE,d]},{cN:"string",b:'"',e:'"',c:[c.BE,d],r:0},{cN:"comment",b:"###",e:"###"},c.HCM,{cN:"regexp",b:"///",e:"///",c:[c.HCM]},{cN:"regexp",b:"//[gim]*"},{cN:"regexp",b:"/\\S(\\\\.|[^\\n])*/[gim]*"},{b:"`",e:"`",eB:true,eE:true,sL:"javascript"},{cN:"function",b:a+"\\s*=\\s*(\\(.+\\))?\\s*[-=]>",rB:true,c:[e,{cN:"params",b:"\\(",e:"\\)"}]},{cN:"class",bWK:true,k:"class",e:"$",i:":",c:[{bWK:true,k:"extends",eW:true,i:":",c:[e]},e]},{cN:"property",b:"@"+a}]}}(hljs);hljs.LANGUAGES.json=function(a){var e={literal:"true false null"};var d=[a.QSM,a.CNM];var c={cN:"value",e:",",eW:true,eE:true,c:d,k:e};var b={b:"{",e:"}",c:[{cN:"attribute",b:'\\s*"',e:'"\\s*:\\s*',eB:true,eE:true,c:[a.BE],i:"\\n",starts:c}],i:"\\S"};var f={b:"\\[",e:"\\]",c:[a.inherit(c,{cN:null})],i:"\\S"};d.splice(d.length,0,b,f);return{c:d,k:e,i:"\\S"}}(hljs);