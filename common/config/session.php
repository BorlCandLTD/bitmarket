<?php defined('SYSPATH') OR die('No direct script access.');

return array(
	'native' => array(
		'name'      => 'session_name',
		'lifetime'  => Date::DAY,
	),
	'cookie' => array(
		'name'      => 'cookie_name',
		'encrypted' => TRUE,
		'lifetime'  => Date::DAY,
	),
	'database' => array(
		'name'      => 'cookie_name',
		'encrypted' => FALSE,
		'lifetime'  => Date::DAY,
		'group'     => 'default',
		'table'     => 'sessions',
		'columns' => array(
			'session_id'  => 'session_id',
			'last_active' => 'last_active',
			'contents'    => 'contents',
		),
		'gc'        => 500,
	),
);