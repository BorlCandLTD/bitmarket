<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * @package  CMS/Common
 * @category Exceptions
 * @author   BorlCand [Andrew Puhovsky]
 */
class CMS_Exception extends Kohana_Exception {}
