<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * Universal pagination widget
 *
 * @package   CMS/Common
 * @category  Controller
 * @author    BorlCand [Andrew Puhovsky]
 */ 
class Controller_Widget_Pagination extends Controller_CMS_Widget_Pagination {}