<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Blog posts category model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Brand extends ORM {

	protected $_has_many = array(
            'items'    => array(
                     'model'       => 'item',
                     'foreign_key' => 'brand_id',
                 ),
            'categories' => array(
                     'model' => 'category',
                     'through' => 'brands_categories',
                ),
        );
	
	public function getArray(){

		return ORM::factory('Category')
			->where('disabled', '=', 0)
			->order_by('sort_order', 'desc')
			->cached(Date::MINUTE)
			->find_all()
			->as_array();

	}
}