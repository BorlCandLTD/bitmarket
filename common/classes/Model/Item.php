<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Blog posts category model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Item extends ORM {

	//public $childs = array();

	//public $meta_title = "META_TITLE";
	//public $meta_description = "META_TITLE";
	//public $meta_keywords = "META_TITLE";


	protected $_belongs_to = array(
      'categories'  => array(
               'model'       => 'category',
               'foreign_key' => 'category_id',
          ),
      'brands'  => array(
               'model'       => 'brand',
               'foreign_key' => 'brand_id',
          ),
    );
        
        public function addItem($arguments = array())
	{
        /*
          Validate this:
            name=
            parent_id=0
            sort_order=0
            disabled=true
            deleted=false
            description=
            keywords= 
        */
        //$user = Model::factory('user');
 
        /*$validation = Validation::factory($arguments)
            ->rule('name', 'not_empty')
            ->rule('parent_id', 'not_empty')
            ->rule('sort_order', 'not_empty')
            ->rule('disabled', 'not_empty')
            ->rule('deleted', 'not_empty')


            ->rule('name', 'regex', array(':value', '/^[a-z_.]++$/iD'))
            ->rule('parent_id', 'regex', array(':value', '/^\d++$/iD'))
            ->rule('sort_order', 'regex', array(':value', '/^\d++$/iD'))
            ->rule('disabled', 'regex', array(':value', '/^(true|false)++$/iD'))
            ->rule('deleted', 'regex', array(':value', '/^(true|false)++$/iD'))
            
            ->rule('name', array($this, 'unique_name'));
 
            /*->rule('password', 'not_empty')
            ->rule('password', 'min_length', array(':value', 6))
            ->rule('confirm',  'matches', array(':validation', ':field', 'password'))
 
            ->rule('use_ssl', 'not_empty')
            ->rule('use_ssl', 'in_array', array(':value', array('yes', 'no')));*/
 
        //if ($validation->check())
        //{
          return array(ORM::factory('Item')
              ->values($arguments, array('name', 'brand_id', 'name', 'category_id', 'disabled', 'deleted', 'unit', 'price', 'count', 'keywords'))
              ->save()
              ->id);
        //}
 
        // Validation failed, collect the errors
        //$errors = $validation->errors('ru');
 
        // Display the registration form
        /*$this->response->body(View::factory('user/register'))
            ->bind('post', $this->request->post())
            ->bind('errors', $errors);*/
        //return $errors;


  }
  public function editItem($arguments = array())
	{
        /*
          Validate this:
            name=
            parent_id=0
            sort_order=0
            disabled=true
            deleted=false
            description=
            keywords= 
        */
        //$user = Model::factory('user');
 
        /*$validation = Validation::factory($arguments)
            ->rule('id', 'not_empty')
            ->rule('name', 'not_empty')
            ->rule('parent_id', 'not_empty')
            ->rule('sort_order', 'not_empty')
            ->rule('disabled', 'not_empty')
            ->rule('deleted', 'not_empty')


            ->rule('id', 'regex', array(':value', '/^[0-9]++$/iD'))
            ->rule('name', 'regex', array(':value', '/^[a-z_.]++$/iD'))
            ->rule('parent_id', 'regex', array(':value', '/^\d++$/iD'))
            ->rule('sort_order', 'regex', array(':value', '/^\d++$/iD'))
            ->rule('disabled', 'regex', array(':value', '/^(true|false)++$/iD'))
            ->rule('deleted', 'regex', array(':value', '/^(true|false)++$/iD'));
 */
        //if ($validation->check())
       // {
          return array(ORM::factory('Item', $arguments['id'])
              ->values($arguments, array('name', 'brand_id', 'name', 'category_id', 'disabled', 'deleted', 'unit', 'price', 'count', 'keywords'))
              ->save()
              ->id);
       // }
 /*
        // Validation failed, collect the errors
        $errors = $validation->errors('ru');
 
        // Display the registration form
        /*$this->response->body(View::factory('user/register'))
            ->bind('post', $this->request->post())
            ->bind('errors', $errors);*/
        //return $errors;


  }
}