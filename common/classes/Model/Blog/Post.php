<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Blog post model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Blog_Post extends Model_CMS_Blog_Post {}