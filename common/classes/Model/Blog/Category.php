<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Blog posts category model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Blog_Category extends Model_CMS_Blog_Category {}