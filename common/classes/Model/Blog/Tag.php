<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Blog post tag
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Blog_Tag extends Model_CMS_Blog_Tag {}