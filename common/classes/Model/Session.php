<?php

defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Blog posts category model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Session extends ORM {

    protected $_has_many = array(
        'orders' => array(
            'model' => 'order',
            'foreign_key' => 'client_id',
        ),
    );
    
}
