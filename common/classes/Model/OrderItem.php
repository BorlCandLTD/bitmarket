<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Blog posts category model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_OrderItem extends ORM {

        protected $_belongs_to = array(
            'order'  => array(
                'model'       => 'order',
                'foreign_key' => 'order_id',
            ),
            'item'  => array(
                'model'       => 'item',
                'foreign_key' => 'item_id',
            )
        );
	
	/*public function getArray(){

		return ORM::factory('Category')
			->where('disabled', '=', 0)
			->order_by('sort_order', 'desc')
			->cached(Date::MINUTE)
			->find_all()
			->as_array();

	}*/
}