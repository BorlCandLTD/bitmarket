<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Info page model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Info_Page extends Model_CMS_Info_Page {}