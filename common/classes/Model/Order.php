<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Blog posts category model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Order extends ORM {

    protected $_has_many = array(
        'orders_items' => array(
            'model' => 'orderitem',
            'foreign_key' => 'order_id',
        ),
    );

    public function addOrder($arguments = array()) {
        
        $order = ORM::factory('Order')
                    ->values($arguments, array('client_id', 'comment'))
                    ->save();
        
        for ($i = 0; $i<count($arguments['items']); $i+=2){
            $order_items[$i/2] = ORM::factory('OrderItem')
                    ->values(
                            array(
                                "order_id" => $order->id, 
                                "item_id" => $arguments['items'][$i], 
                                "count" => $arguments['items'][$i+1],
                                "price" => ORM::factory('Item')->where("id","=",Arr::get($arguments['items'], $i))->find()->price
                            )
                        )->save()->id;
        }
        return array($order_items);
        
    }
}
