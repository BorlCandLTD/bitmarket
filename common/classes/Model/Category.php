<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Category model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Category extends ORM {

    //public $meta_title = "META_TITLE";
    //public $meta_description = "META_TITLE";
    //public $meta_keywords = "META_TITLE";


    protected $_has_many = array(
        'items' => array(
            'model' => 'item',
            'foreign_key' => 'category_id',
        ),
        'brands' => array(
            'model' => 'brand',
            'through' => 'brands_categories',
        ),
    );
    protected $_belongs_to = array(
        'parent' => array(
            'model' => 'category',
            'foreign_key' => 'parent_id',
        )
    );

    public function getArray() {

        return ORM::factory('Category')
                        ->where('disabled', '=', 0)
                        ->order_by('sort_order', 'desc')
                        ->cached(Date::MINUTE)
                        ->find_all()
                        ->as_array();
    }

    public static function unique_name($name) {
        return !DB::select(array(DB::expr('COUNT(name)'), 'total'))
                        ->from('categories')
                        ->where('name', '=', $name)
                        ->execute()
                        ->get('total');
    }

    public function addCategory($arguments = array()) {
        /*
          Validate this:
          name=
          parent_id=0
          sort_order=0
          disabled=true
          deleted=false
          description=
          keywords=
         */
        //$user = Model::factory('user');

        $validation = Validation::factory($arguments)
                ->rule('name', 'not_empty')
                ->rule('parent_id', 'not_empty')
                ->rule('sort_order', 'not_empty')
                ->rule('disabled', 'not_empty')
                ->rule('deleted', 'not_empty')
                ->rule('name', 'regex', array(':value', '/^[a-z_.]++$/iD'))
                ->rule('parent_id', 'regex', array(':value', '/^\d++$/iD'))
                ->rule('sort_order', 'regex', array(':value', '/^\d++$/iD'))
                ->rule('disabled', 'regex', array(':value', '/^(true|false)++$/iD'))
                ->rule('deleted', 'regex', array(':value', '/^(true|false)++$/iD'))
                ->rule('name', array($this, 'unique_name'));

        /* ->rule('password', 'not_empty')
          ->rule('password', 'min_length', array(':value', 6))
          ->rule('confirm',  'matches', array(':validation', ':field', 'password'))

          ->rule('use_ssl', 'not_empty')
          ->rule('use_ssl', 'in_array', array(':value', array('yes', 'no'))); */

        if ($validation->check()) {
            return array(ORM::factory('Category')
                        ->values($arguments, array('name', 'parent_id', 'sort_order', 'disabled', 'deleted', 'description', 'keywords'))
                        ->save()
                ->id);
        }

        // Validation failed, collect the errors
        $errors = $validation->errors('ru');

        // Display the registration form
        /* $this->response->body(View::factory('user/register'))
          ->bind('post', $this->request->post())
          ->bind('errors', $errors); */
        return $errors;
    }

    public function editCategory($arguments = array()) {
        /*
          Validate this:
          name=
          parent_id=0
          sort_order=0
          disabled=true
          deleted=false
          description=
          keywords=
         */
        //$user = Model::factory('user');

        $validation = Validation::factory($arguments)
                ->rule('id', 'not_empty')
                ->rule('name', 'not_empty')
                ->rule('parent_id', 'not_empty')
                ->rule('sort_order', 'not_empty')
                ->rule('disabled', 'not_empty')
                ->rule('deleted', 'not_empty')
                ->rule('id', 'regex', array(':value', '/^[0-9]++$/iD'))
                ->rule('name', 'regex', array(':value', '/^[a-z_.]++$/iD'))
                ->rule('parent_id', 'regex', array(':value', '/^\d++$/iD'))
                ->rule('sort_order', 'regex', array(':value', '/^\d++$/iD'))
                ->rule('disabled', 'regex', array(':value', '/^(true|false)++$/iD'))
                ->rule('deleted', 'regex', array(':value', '/^(true|false)++$/iD'));

        if ($validation->check()) {
            return array(ORM::factory('Category', $arguments['id'])
                        ->values($arguments, array('name', 'parent_id', 'sort_order', 'disabled', 'deleted', 'description', 'keywords'))
                        ->save()
                ->id);
        }

        // Validation failed, collect the errors
        $errors = $validation->errors('ru');

        // Display the registration form
        /* $this->response->body(View::factory('user/register'))
          ->bind('post', $this->request->post())
          ->bind('errors', $errors); */
        return $errors;
    }

    /* $post = Validation::factory($arguments)
      //->filter(TRUE, 'trim')

      //->filter('name', 'strtolower')

      ->rule('name', 'not_empty')
      ->rule('name', 'regex', array('/^[a-z_.]++$/iD'));
      //->rule('name', array($user, 'unique_username'))

      //->rule('password', 'not_empty')
      //->rule('password', 'min_length', array('6'))
      //->rule('confirm',  'matches', array('password'))

      //->rule('use_ssl', 'not_empty')
      //->rule('use_ssl', 'in_array', array(array('yes', 'no')))

      //->callback('password', array($user, 'hash_password'));

      if ($post->check())
      {
      // Data has been validated, register the user
      //$user->register($post);
      return array('result' => 'success',
      'message' => 'new id LOL');
      // Always redirect after a successful POST to prevent refresh warnings
      //$this->request->redirect('user/profile');
      }
      return array('result' => 'error',
      'message' => 'new id LOL'); */
}
