<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Blog posts category model
 * 
 * @package   CMS/Common
 * @category  Model
 * @author    BorlCand [Andrew Puhovsky]
 */
class Model_Language extends ORM {

 	public function getValue($string, $lang)
 	{

 		$value = ORM::factory('Language')
		 			->where('lang', '=', $lang)
		 			->where('token', '=', $string)
		 			->find();
		//throw new CMS_Exception($value->value);
		
		return $value->loaded() ? $value->value : '';
 	}

 	public function appendValue($string, $lang)
 	{
 		$value = ORM::factory('Language');
 		$value->lang = $lang;
 		$value->token = $string;
 		$value->value = $string;
 		$value->save();
 	}
}